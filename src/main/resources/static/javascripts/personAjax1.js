(function ($) {
    $(document).ready(function () {
        var API_URL1 = '/admin/owners/api/search/email';
        var error_message1 = 'No matches found in Database.';
        var $tableSelector1 = $('#personSearch');
        var $searchForm1 = $('#searchForm1');
        var $tableBody1 = $tableSelector1.find('tbody');

        function getItems(success, error) {
            $.ajax({
                url: API_URL1,
                method: 'GET',
                data: $searchForm1.serialize()
            }).done(function (response) {
                success(response);
            }).fail(function () {
                error();
                console.log(response);
            });
        }

        function renderRowErrorMessage() {
            var $row1 = $('<tr></tr>');
            var $cell1 = $('<td></td>').text(error_message1);
            $row1.append($cell1);
            $tableBody1.append($row1);
        }

        function renderTable(results) {

            var $columnsOrder = ['personFirstName', 'personLastName', 'personAfm',
                'personAddress','personEmail', 'vehicleBrand',  'vehiclePlate' ,'personType','personAfm','personId']
            var item1 = results;
            var $tr1 = $('<tr></tr>');
            $columnsOrder.forEach(function (column,index) {

                var $personFirstName1=$('<td>'+item1[column]+'</td>');
                if (index===$columnsOrder.length-2){
                    $tr1.append('<td><a class = "btn btn-sm btn-success button-edit" href="/admin/owners/edit/'+item1.personAfm+'">Edit</a></td>');
                }else if(index===$columnsOrder.length-1){
                    $tr1.append('<td><a class = "btn btn-sm btn-danger button-delete" href="/admin/owners/delete/'+item1.personId+'">Delete</a></td>');

                }else {

                    $tr1.append($personFirstName1);
                }


            })
            console.log(results);
            $tableBody1.html($tr1);
            $tableSelector1.removeClass('hidden');
            // var item1 = results;
            // var $tr1 = $('<tr></tr>');
            // // var $personId=$('<td>' + item.personId+'</td>');
            // var $personFirstName1=$('<td>'+item1.personFirstName+'</td>');
            // var $personLastName1=$('<td>'+item1.personLastName+'</td>');
            // var $personAfm1=$('<td>'+item1.personAfm+'</td>');
            // var $personAddress1=$('<td>'+item1.personAddress+'</td>');
            // var $personEmail1=$('<td>'+item1.personEmail+'</td>');
            // var $vehicleBrand1=$('<td>'+item1.vehicleBrand+'</td>');
            // var $vehiclePlate1=$('<td>'+item1.vehiclePlate+'</td>');
            // var $personType1=$('<td>'+item1.personType+'</td>');
            // $tr1.append($personFirstName1);
            // $tr1.append($personLastName1);
            // $tr1.append($personAfm1);
            // $tr1.append($personAddress1);
            // $tr1.append($personEmail1);
            // $tr1.append($vehicleBrand1);
            // $tr1.append($vehiclePlate1);
            // $tr1.append($personType1);
            // $tr1.append('<td><a class = "btn btn-sm btn-success button-edit" href="/admin/owners/edit/'+item1.personAfm+'">Edit</a></td>');
            // $tr1.append('<td><a class = "btn btn-sm btn-danger button-delete" href="/admin/owners/delete/'+item1.personId+'">Delete</a></td>');

            // console.log(results);
        }

        if ($tableSelector1.length > 0) {
            $searchForm1.submit(function (e) {
                e.preventDefault();

                getItems(function (listOfItems) {
                    renderTable(listOfItems);
                }, function () {
                    renderRowErrorMessage();
                });

            });
        }

    });

})(jQuery);