
if($('#repairRegisterForm').length>0){

    $.ajax({
        method: "get",
        url: "/admin/owners/api/search",
        data: {afm: "John", email: "012235"}
    })
        .done(function (message) {
            alert(message);
        });

}


jQuery(function ($) {

    function validateRepairCreate() {
        $('#create-repair').validate({

            rules: {
                dateFrom: {
                    required: true,
                },
                status: {
                    required: true
                },
                fix_type: {
                    required: true
                },
                afm: {
                    required: true
                },
                cost:{
                    required:true
                },
            },
            messages: {
                dateListing: {
                    required: 'Please enter the Starting Date'
                },
                status:{
                    required: 'Please enter the status of the Repair'
                },
                fix_type: {
                    required: 'Please enter the Repair Type'
                },
                afm: {
                    required: 'Afm cannot be empty.'
                },
                cost:{
                    required: 'You have to set the estimated cost of the repair.'
                }
            }
        });
    }
    validateRepairCreate();

        function validateCreateUser() {
            $('#create-user').validate({

                rules: {
                    firstname: {
                        required: true,
                    },
                    lastname: {
                        required: true
                    },
                    vehicleBrand: {
                        required: true
                    },
                    licencePlate: {
                        required: true
                    },
                    afm: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    confirm_password: {
                        equalTo: '#password'
                    },
                    email: {
                        required: true,
                        email:true
                    },

                },
                messages: {
                    firstname: {
                        required: 'The first name cannot be empty.'
                    },
                    lastname: {
                        required: 'The last name cannot be empty.'
                    },
                    vehicleBrand: {
                        required: 'Please enter the Vehicle\'s brand.'
                    },
                    licencePlate: {
                        required: 'Please enter the Licence Plate of the Vehicle.'
                    },
                    afm: {
                        required: 'Afm cannot be empty.'
                    },
                    password: {
                        required: 'Password cannot be empty.'
                    },
                    email: {
                        required: 'The email cannot be empty.',
                        email:'Please enter a valid email.'
                    },
                }
            });
        }
        validateCreateUser();

    if($('input#dateFrom').length>0) {
        $('input#dateFrom').datepicker({
            format: 'yyyy-mm-dd'
        });
    }

    if ($('.button-delete-confirmation')) {

        $('.button-delete-confirmation').click(function (e) {

            $('#dialog-delete-confirmation').dialog({
                autoOpen: false,
                modal: true
            });
        });
    }

});
