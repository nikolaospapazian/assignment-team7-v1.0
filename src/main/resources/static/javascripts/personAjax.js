(function ($) {
    $(document).ready(function () {
    var API_URL = '/api/admin/owners/search';
    var error_message = 'No matches found in Database.';
    var $tableSelector = $('#personSearch');
    var $searchForm = $('#searchForm');
    var $tableBody = $tableSelector.find('tbody');

    function getItems(success, error) {
        $.ajax({
            url: API_URL,
            method: 'GET',
            data: $searchForm.serialize()
        }).done(function (response) {
            success(response);
        }).fail(function () {
            error();
            console.log(response);
        });
    }

    function renderRowErrorMessage() {
        var $row = $('<tr></tr>');
        var $cell = $('<td></td>').text(error_message);
        $row.append($cell);
        $tableBody.html($row);
    }

    function renderTable(results) {
        console.log(results)
        var item = results;
        var $tr = $('<tr></tr>');
        // var $personId=$('<td>' + item.personId+'</td>');
        var $personFirstName=$('<td>'+item.personFirstName+'</td>');
        var $personLastName=$('<td>'+item.personLastName+'</td>');
        var $personAfm=$('<td>'+item.personAfm+'</td>');
        var $personAddress=$('<td>'+item.personAddress+'</td>');
        var $personEmail=$('<td>'+item.personEmail+'</td>');
        var $vehicleBrand=$('<td>'+item.vehicleBrand+'</td>');
        var $vehiclePlate=$('<td>'+item.vehiclePlate+'</td>');
        var $personType=$('<td>'+item.personType+'</td>');
        $tr.append($personFirstName);
        $tr.append($personLastName);
        $tr.append($personAfm);
        $tr.append($personAddress);
        $tr.append($personEmail);
        $tr.append($vehicleBrand);
        $tr.append($vehiclePlate);
        $tr.append($personType);
        $tr.append('<td><a  href="/admin/owners/edit/'+item.personAfm+'" title="Edit"><span data-feather="edit"></span></a></td>');
        $tr.append('<td><a  href="/admin/owners/delete/'+item.personId+'"title="Delete"><span data-feather="delete"></span></a></td>');
        $tableBody.html($tr);
        $tableSelector.removeClass('hidden');
        // console.log(results);

        feather.replace();


    }

    if ($tableSelector.length > 0) {
        $searchForm.submit(function (e) {
            e.preventDefault();

            getItems(function (listOfItems) {
                renderTable(listOfItems);
            }, function () {
                renderRowErrorMessage();
            });

        });
    }

});

})(jQuery);
