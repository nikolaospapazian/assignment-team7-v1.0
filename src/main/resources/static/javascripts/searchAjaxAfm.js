(function ($) {
    $(document).ready(function () {
        var API_URL = '/api/admin/repairs/search';
        var error_message = 'No matches found in Database.';
        var $tableSelector = $('#personSearch');
        var $searchForm = $('#searchForm');

        var $tableBody = $tableSelector.find('tbody');

        function getItems(success, error) {
            $.ajax({
                url: API_URL,
                method: 'GET',
                data: $searchForm.serialize()
            }).done(function (response) {
                if(response==null) { throw renderRowErrorMessage(); }
                success(response);
            }).fail(function () {
                error();
                console.log(response);
            });
        }

        function renderRowErrorMessage() {
            var $row = $('<tr></tr>');
            var $cell = $('<td></td>').text(error_message);
            $row.append($cell);
            $tableBody.html($row);
        }

        function renderTable(items) {
            console.log(items)
            var documentFragment = $(document.createDocumentFragment());
            items.forEach(function (item) {

            var $tr = $('<tr></tr>');
            var $id=$('<td>'+item.id+'</td>');
            var $fix_type=$('<td>'+item.fix_type+'</td>');
            var $stage=$('<td>'+item.stage+'</td>');
            var $price=$('<td>'+item.price+'€</td>');
            var $description=$('<td>'+item.description+'</td>');
            var $dayStart=$('<td>'+item.dayStart+'</td>');
            var $personAfm=$('<td>'+item.personAfm+'</td>');
            var $vehiclePlate=$('<td>'+item.vehiclePlate+'</td>');
            var $firstName=$('<td>'+item.firstName+'</td>');
            var $lastName=$('<td>'+item.lastName+'</td>');
            $tr.append($firstName);
            $tr.append($lastName);
            $tr.append($personAfm);
            $tr.append($fix_type);
            $tr.append($stage);
            $tr.append($price);
            $tr.append($dayStart);
            $tr.append($vehiclePlate);
            $tr.append($description);
            $tr.append('<td><a  href="/admin/repair/edit/'+item.id+'" title="Edit"><span data-feather="edit"></span></a></td>');
            $tr.append('<td><a href="/admin/repairs/delete/'+item.id+'"title="Delete"><span data-feather="delete"></span></a></td>');
                documentFragment.append($tr);
            });
            $tableBody.html(documentFragment);
            $tableSelector.removeClass('hidden');
            console.log(items);
            feather.replace();
        }

        if ($tableSelector.length > 0) {
            $searchForm.submit(function (e) {
                e.preventDefault();

                getItems(function (listOfItems) {
                    renderTable(listOfItems);
                }, function () {
                    renderRowErrorMessage();
                });

            });
        }

    });

})(jQuery);