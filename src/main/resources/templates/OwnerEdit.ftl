<#import "/spring.ftl" as spring />
<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <!-- Custom styles for this template -->
    <link href="/styles/adminOwner.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">
        <#include  "partials/sidebar.ftl">


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">${error111!"Update User "}  </h1>

                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <#--<a href="/owners/register"><button class="btn btn-sm btn-outline-secondary" >Create</button></a>-->
                    </div>
                </div>
            </div>

            <#--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>-->


            <main class="my-form">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">Register</div>
                                <div class="card-body">
                                    <form name="create-user" id="create-user" onsubmit="" action="/admin/owners/edit/${userRegisterForm.id!""}" method="post">

                                        <div class="form-group row">
                                            <label for="firstname" class="col-md-2 col-form-label text-md-right">First Name</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.firstname"/>
                                                    <input type="text" id="firstname" value="${userRegisterForm.firstname!""}" class="form-control" name="firstname">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lastname" class="col-md-2 col-form-label text-md-right">Last Name</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.lastname"/>
                                                    <input type="text" id="lastname" value="${userRegisterForm.lastname!""}" class="form-control" name="lastname">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="address" class="col-md-2 col-form-label text-md-right">Address</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.address"/>
                                                    <input type="text" id="address" value="${userRegisterForm.address!""}" class="form-control" name="address">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="afm" class="col-md-2 col-form-label text-md-right">Afm</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.afm"/>
                                                    <input type="text" id="afm" value="${userRegisterForm.afm!""}" class="form-control" name="afm">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="vehicleBrand" class="col-md-2 col-form-label text-md-right">Vehicle Brand</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.vehicleBrand"/>
                                                    <input type="text" id="vehicleBrand" value="${userRegisterForm.vehicleBrand!""}" class="form-control" name="vehicleBrand">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="licencePlate" class="col-md-2 col-form-label text-md-right">Vehicle License Plate</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.licencePlate"/>
                                                    <input type="text" id="licencePlate" value="${userRegisterForm.licencePlate!""}" class="form-control" name="licencePlate">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="userType" class="col-md-2 col-form-label text-md-right">User Type</label>
                                            <div class="col-md-8">


                                                <@spring.bind "userRegisterForm.userType"/>
                                                    <label class="radio-inline"><input type="radio" value="USER" name="userType" <#if "${userRegisterForm.userType}"=="USER">checked</#if> >User</label>
                                                    <label class="radio-inline"><input type="radio" value="ADMIN" name="userType" <#if "${userRegisterForm.userType}"=="ADMIN">checked</#if> >Admin</label>
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-md-2 col-form-label text-md-right">E-Mail</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.email"/>
                                                    <input type="text" id="email" value="${userRegisterForm.email!""}" class="form-control" name="email">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password" class="col-md-2 col-form-label text-md-right">Password</label>
                                            <div class="col-md-8">
                                                <@spring.bind "userRegisterForm.password"/>
                                                    <input type="password" id="password" value="${userRegisterForm.password!""}"  class="form-control" name="password">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="col-md-4 offset-md-2">
                                            <button type="reset" class="btn btn-default">
                                                Reset
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Register
                                            </button>
                                        </div>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </main>
            <br/>
    </div>
    </main>
</div>
</div>

<#include "partials/scripts.ftl">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="/javascripts/app.js"></script>
</body>
</html>
