<#import "/spring.ftl" as spring />
<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

    <!-- Custom styles for this template -->
    <link href="/styles/adminOwner.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">
        <#include  "partials/sidebar.ftl">


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">${error111!"Update Repair"} </h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                    </div>
                </div>
            </div>



            <main class="my-form">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">Register</div>
                                <div class="card-body">
                                    <form name="" id="create-repair" onsubmit="" action="/admin/repair/edit/${repairRegisterForm.id}" method="post">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right" for="dateFrom">Date of Arrival:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.dateFrom"/>
                                                <input type="text"  class="form-control" id="dateFrom" value="${repairRegisterForm.dateFrom!""}" name="dateFrom" placeholder="yyyy-mm-dd">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right" for="dateFrom">Starting date of repair:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.dateStart"/>
                                                <input type="text"  class="form-control" id="dateFrom" value="${repairRegisterForm.dateStart!""}" name="dateStart" placeholder="yyyy-mm-dd">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right" for="dateTo">Ending date of repair:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.dateTo"/>
                                                <input type="text"  class="form-control" id="dateFrom" value="${repairRegisterForm.dateTo!""}" name="dateTo" placeholder="yyyy-mm-dd">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right" for="fix_type">Type of Repair</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.fix_type"/>
                                                <select class="form-control" id="fix_type" name="fix_type">
                                                    <option value="SMALL"<#if "${repairRegisterForm.fix_type}"=="SMALL"> selected</#if> > Small Repair</option>
                                                    <option value="BIG"<#if "${repairRegisterForm.fix_type}"=="BIG"> selected</#if>>Big Repair</option>
                                                    <#list spring.status.errorMessages as error>
                                                        <span>${error}</span>
                                                    </#list>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right" for="fix_type">Status of Repair</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.stage"/>

                                                <select class="form-control" id="stage" name="stage">
                                                       <option value="IN_QUEUE"  <#if "${repairRegisterForm.stage}"=="IN_QUEUE"> selected </#if> >In queue </option>
                                                       <option value="IN_PROGRESS" <#if "${repairRegisterForm.stage}"=="IN_PROGRESS"> selected </#if> >In progress</option>
                                                       <option value="COMPLETED" <#if "${repairRegisterForm.stage}"=="IN_PROGRESS"> selected </#if> >Completed </option>
                                                     <#list spring.status.errorMessages as error>
                                                        <span>${error}</span>
                                                     </#list>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="afm" class="col-md-2 col-form-label text-md-right">Owner's AFM:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.afm"/>
                                                <input type="text" id="afm" value="${repairRegisterForm.afm!""}" name="afm" class="form-control">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="sprice" class="col-md-2 col-form-label text-md-right">Estimated Cost:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.sprice"/>
                                                <input type="text" id="sprice" name="sprice" value="${repairRegisterForm.sprice!""}" class="form-control">
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="description" class="col-md-2 col-form-label text-md-right">Description:</label>
                                            <div class="col-md-8">
                                                <@spring.bind "repairRegisterForm.description"/>
                                                <textarea class="form-control" cols="30" id="description"  name="description" placeholder="Description"
                                                          rows="5"> ${repairRegisterForm.description}</textarea>
                                                <#list spring.status.errorMessages as error>
                                                    <span>${error}</span>
                                                </#list>
                                            </div>
                                        </div>



                                        <div class="  col-md-4 offset-md-2">
                                            <button type="reset" class="btn btn-default">
                                                Reset
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Update
                                            </button>
                                        </div>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </main>


            <br/>
    </div>
    </main>
</div>
</div>


<#include "partials/scripts.ftl">


<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="/javascripts/app.js"></script>


</body>
</html>
