<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Our First Spring MVC Application">
<meta name="author" content="Team7">

<title>Welcome to our Spring Mvc Application</title>
<!-- Bootstrap core CSS -->
<link rel="icon" href="/styles/team7.ico">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
