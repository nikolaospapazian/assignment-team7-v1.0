<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <link href="/styles/adminOwner.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">

        <#include  "partials/sidebar.ftl">


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">User's Catalogue</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a href="/admin/owners/register"><button class="btn btn-sm btn-outline-secondary" >Create</button></a>

                    </div>
                </div>
            </div>

            <h2>        </h2>


                    <main class="my-form">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-10">
                                    <div class="card">
                                        <div class="card-header">Search User</div>
                                        <div class="card-body">
                                            <form name="searchForm" id="searchForm" onsubmit="" action="/api/admin/owners/search" method="get">
                                                <div class="form-group row">
                                                    <label for="afm" class="col-md-3 col-form-label text-md-right">Search by AFM</label>
                                                    <div class="col-md-6  input-group">
                                                        <input type="text" id="afm" class="form-control" name="afm">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="email" class="col-md-3 col-form-label text-md-right">Search by E-Mail Address</label>
                                                    <div class="col-md-6 ">
                                                         <input type="text" id="email" class="form-control" name="email">
                                                    </div>

                                                </div>
                                                <div class="col-md-6 offset-md-3">
                                                <button type="submit" class="btn btn-primary">
                                                    Search
                                                </button>
                                                    </div>
                                                </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


        </main>
            <br/>
            <div id="personSearch"  class="table-responsive hidden">
                  <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>AFM</th>
                        <th>Address</th>
                        <th>Email Address</th>
                        <th>Vehicle's Brand</th>
                        <th>Vehicle's Plate</th>
                        <th>User Type</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
    </div>
</div>

<#include "partials/scripts.ftl">

<script src="/javascripts/personAjax.js"></script>

</body>
</html>
