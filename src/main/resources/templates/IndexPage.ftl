<!doctype html>
<html lang="en" class="bg-team index">
<head>
    <#include "partials/head.ftl">

    <!-- Custom styles for this template -->
    <link href="/styles/adminHome.css" rel="stylesheet">
</head>

<body  >
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/admin/home"><img src="/styles/team7tr.jpg" alt="" width=100% height=""></a>

</nav>
<div class="container-fluid">
    <div class="row">


        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4"  >
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom" >
                <h1 class="h1">Our Very First Spring MVC Web Application</h1>
            </div>

            <div class="text-center col-lg-3 lg-button" >
            <a href="/login"><button type="button" class="btn btn-primary btn-block btn-lg">Please Login</button></a>
            </div>
        </main>

    </div>
</div>



</body>
</html>
