<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

    <!-- Custom styles for this template -->
    <link href="/styles/adminOwner.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">
        <#include  "partials/sidebar.ftl">


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Repair's Catalogue</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a href="/admin/repairs/register"><button class="btn btn-sm btn-outline-secondary" >Create</button></a>
                    </div>
                </div>
            </div>

            <#--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>-->

            <h2>        </h2>


            <main class="my-form">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">Search Repair</div>
                                <div class="card-body">
                                    <form id="searchForm" name="searchForm" onsubmit="" action="/api/admin/repairs/search" method="get">

                                        <div class="form-group row">
                                            <label for="dateFrom" class="col-md-3 col-form-label text-md-right">Uncompleted Repairs on Date</label>
                                            <div class="col-md-6">
                                                <input type="text" id="dateFrom" class="form-control" name="dateFrom" placeholder="yyyy-mm-dd">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="afm" class="col-md-3 col-form-label text-md-right">Search by AFM</label>
                                            <div class="col-md-6  input-group">
                                                <input type="text" id="afm" class="form-control" name="afm">

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="SearchPlate" class="col-md-3 col-form-label text-md-right">Search by License Plate</label>
                                            <div class="col-md-6">
                                                <input type="text" id="SearchPlate" class="form-control" name="SearchPlate">
                                            </div>
                                        </div>


                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Search
                                            </button>
                                        </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </main>
            <br/>
            <div id="personSearch" class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Owner's Name</th>
                        <th>Owner's Lastname</th>
                        <th>Owner's Afm</th>
                        <th>Repair Type</th>
                        <th>Repair Status</th>
                        <th>Estimated Cost</th>
                        <th>Starting Date</th>
                        <th>Licence Plate</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>



                    </tr>
                    </tbody>
                </table>
            </div>
    </div>
</div>


<#include "partials/scripts.ftl">

<script src="/javascripts/searchAjaxAfm.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="/javascripts/app.js"></script>



</body>
</html>
