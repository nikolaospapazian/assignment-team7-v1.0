<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <!-- Custom styles for this template -->
    <link href="/styles/ownerHome.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">

        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Repairs status</h1>
            </div>


            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Licence Plate</th>
                        <th>Repair Type</th>
                        <th>Repair Status</th>
                        <th>Starting Date</th>
                        <th>Cost of Repair</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list repairs as repair>
                        <tr>
                        <td>${repair.vehiclePlate!""}</td>
                        <td>${repair.fix_type!""}</td>
                        <td> ${repair.stage!""}</td>
                        <td> ${repair.dayStart!""}</td>
                        <td> ${repair.price!""}€</td>
                        <td> ${repair.description!""}</td>
                    </#list>
                </table>
            </div>
        </main>
    </div>
</div>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

</body>
</html>
