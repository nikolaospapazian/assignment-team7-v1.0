<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">

    <!-- Custom styles for this template -->
    <link href="/styles/adminHome.css" rel="stylesheet">
</head>

<body>
<#include "partials/navbar.ftl">

<div class="container-fluid">
    <div class="row">
        <#include  "partials/sidebar.ftl">


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Registration could not be completed. Please try again.</h1>
            </div>


    </div>
</div>

<#include "partials/scripts.ftl">


</body>
</html>
