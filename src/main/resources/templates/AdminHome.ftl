<!doctype html>
<html lang="en">
  <head>
    <#include "partials/head.ftl">
    <
    <!-- Custom styles for this template -->
    <link href="/styles/adminHome.css" rel="stylesheet">
  </head>

  <body>

  <#include "partials/navbar.ftl">

    <div class="container-fluid">
      <div class="row">

        <#include  "partials/sidebar.ftl">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Upcoming Repairs</h1>
          </div>


          <div class="table-responsive ">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>Owner's Afm</th>
                  <th>Licence Plate</th>
                  <th>Repair Type</th>
                  <th>Repair Status</th>
                  <th>Starting Date</th>
                  <th>Cost of Repair</th>
                  <th>Description</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <#list repairs as repair>
                <tr>
                <td>${repair.afm!""}</td>
                <td>${repair.vehiclePlate!""}</td>
                <td>${repair.fix_type!""}</td>
                <td> ${repair.stage!""}</td>
                <td> ${repair.dayStart!""}</td>
                <td> ${repair.price!""}€</td>
                <td> ${repair.description!""}</td>
                <td>
              <a href="/admin/repair/edit/${repair.id!""}" title="Edit"><span data-feather="edit"></span></a>
                </td>
                <td>
              <a href="/admin/repairs/delete/${repair.id!""}" title="Delete"><span data-feather="delete"></span></a>
                </td>
                </tr>
              </#list>
                </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>

  <#include "partials/scripts.ftl">

  </body>
</html>
