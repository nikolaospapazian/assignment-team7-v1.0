package com.team7.project.service;


import com.team7.project.dao.DaoPersonCreateUpdate;
import com.team7.project.dao.DaoRepairCreateUpdate;
import com.team7.project.dao.DaoRepairGet;
import com.team7.project.exception.PersonNotFoundException;
import com.team7.project.exception.RepairNotfoundException;
import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nikolaos Papazian
 * RepairServiceImpl implements the RepairService
 * used to get or set properties from ServiceRepositoryRepairsTransactionFunction
 * and with using the dtoRepairs.${}.Class sent or get to Repository Repairs
 */
@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private DaoRepairCreateUpdate repairDAO;
    @Autowired
    private DaoPersonCreateUpdate personDao;
    @Autowired
    private PersonService personService;
    @Autowired
    private DaoRepairGet daoRepairGet;

    @Override
    public RepairDtoUpdate getRepairForUpdate(Long id) {
        return daoRepairGet.getRepairForUpdate(id);
    }

    /**
     * getRepairsList
     *
     * @return RepairDto list with all repairs
     */
    @Override
    public List<RepairDto> getRepairsList() {

        return daoRepairGet.getRepairsList();
    }


    public List<RepairDto> getRepairsByEmail(String email){
        return daoRepairGet.getRepairsByEmail(email);
    }

    /**
     * getFirstTenRepairs
     *
     * @return RepairDto List with 10 repairs
     */
    public List<RepairDto> getFirstTenRepairs() {
        return repairDAO.findFirstTen();
    }

    /**
     * getRepairs
     *
     * @param id id from repair
     * @return RepairDto
     */
    @Override
    public RepairDto getRepairs(Long id) {
        return daoRepairGet.getRepairs(id);
    }

    /**
     * findRepairByAfm
     * find all Repairs for that person from afm
     * @param afm person
     * @return RepairAfm
     */
    public List<RepairDto> findRepairByAfm(Long afm) {
        List<RepairDto> repairDto = new ArrayList<>();
        if (afm != null) {
            repairDto = daoRepairGet.getRepairsByAfm(personDao.FindPersonByAfmAll(afm));
        }
        return repairDto;
    }

    /**
     * setNewRepair
     * Ger Repair Dto And Find Person And After That Insert New Repair
     *
     * @param repairDtoCreate get the RepairDtoCreate
     */
    @Override
    public void setNewRepair(RepairDtoCreate repairDtoCreate)
    {
        if(repairDtoCreate.getAfm()!=null) {
            repairDAO.setNewRepair(repairDtoCreate,personService.getPersonAllByAfm(repairDtoCreate.getAfm()));
       }else {
            throw new PersonNotFoundException("Repair with afm: "+repairDtoCreate.getAfm()+"  or  Email: "+repairDtoCreate.getEmail()+"not exist");
        }
    }


    /**
     * updateRepair
     * @param repairDtoUpdate and update this repair
     */
    @Override
    public void updateRepair(RepairDtoUpdate repairDtoUpdate){
        if(repairDtoUpdate.getAfm()!=null) {
            repairDAO.updateRepair(repairDtoUpdate,personService.getPersonAllByAfm(repairDtoUpdate.getAfm()));
        }else{
            throw new PersonNotFoundException("Repair with afm: "+repairDtoUpdate.getAfm()+"  or  Email: "+repairDtoUpdate.getEmail()+"not exist");
        }
    }

    /**
     * deleteRepair
     * @param id for that repair
     */
    public void deleteRepair(Long id) {
        if (getRepairs(id) != null) {
            repairDAO.deleteRepair(id);
        } else {
            throw new PersonNotFoundException("Repair with id: " + id + " not exist");
        }
    }


    /**
     * findRepairsToShowToday
     * @return RepairDto List to Show in the home page of Admin
     */
    @Override
    public List<RepairDto> findRepairsToShowToday() {
        List<RepairDto> repairDtos = daoRepairGet.findRepairsBeforeDay(LocalDate.now().plusDays(1L));
        repairDtos.forEach(repairDto -> repairDto.setAfm(Long.toString(repairDto.getPersonAfm())));
        return repairDtos;
    }

    /**
     * findRepairsToShowSpecificDay
     * @return RepairDto List with Repairs is open that Day
     */
    @Override
    public List<RepairDto> findRepairsToShowSpecificDay(LocalDate specificDay) {
        return daoRepairGet.findRepairsBeforeDay(specificDay.plusDays(1L));
    }

    /**
     * findRepairsToShowUntilToday
     * @return RepairDto List to Show in the home page of Admin
     */
    @Override
    public List<RepairDto> findRepairsToShowUntilToday() {
        return daoRepairGet.findRepairsBeforeDayUntilToday(LocalDate.now());
    }

    /**
     * findRepairsUpcoming
     * @return RepairDto List With Upcoming Repairs
     */
    @Override
    public List<RepairDto> findRepairsUpcoming() {
        return daoRepairGet.findRepairsAfterDay(LocalDate.now());
    }

    @Override
    public List<RepairDto> findRepairsByVehiclePlate(String plate) {
        List<RepairDto> repairDto = new ArrayList<>();
        if (plate != null) {
            repairDto = daoRepairGet.getRepairsByAfm(personDao.FindPersonByPlate(plate));
        }
        return repairDto;

    }

    /*public List<RepairDto> findRepairsByAfmAndDateBeforeDayToAndPlate (Long afm, LocalDate date, String plate) {
        List<RepairDto> repairDto = new ArrayList<>();
        if (afm!=null && date!=null && plate!=null){
            repairDto = daoRepairGet.findRepairsByAfmAndDateBeforeDayToAndPlate(personDao.FindPersonByAfmAll(afm),personDao.FindPersonByPlate(plate),personDao.FindPersonByDateBeforeDayTo)
        }
        return repairDto;
    }*/
    public List<RepairDto> findRepairsByAfmAndDate(String afm, LocalDate date) {
        List<RepairDto> repairDto = new ArrayList<>();
        if (afm != null && date != null) {

            repairDto = daoRepairGet.findRepairsByAfmAndDate(Long.parseLong(afm), date);
        }
        return repairDto;
    }

    public List<RepairDto> findRepairsByPlateAndDate(String plate, LocalDate date) {
        List<RepairDto> repairDto = new ArrayList<>();
        if (plate !=null && date!= null){

            repairDto = daoRepairGet.findRepairsByPlateAndDate(plate, date);
        }

        return repairDto;
    }

    public  List<RepairDto> searchRepairsByAfmAndByDateAndByPlate (String afm, String plate, String date){
        List<RepairDto> repairDto = new ArrayList<>();

        if (!afm.equals("null")) {
            if (!date.equals("")){
                return findRepairsByAfmAndDate(afm,LocalDate.parse(date));
            }
            return findRepairByAfm(Long.parseLong(afm));
        }
        else if (!plate.isEmpty())
        {
            if (!date.equals("")){
                return findRepairsByPlateAndDate(plate,LocalDate.parse(date));
            }
            return findRepairsByVehiclePlate(plate);
        }
        else if(!date.equals("")){
            return findRepairsToShowSpecificDay(LocalDate.parse(date));
        }
        else throw new RepairNotfoundException("Bad Request");

    }
}
