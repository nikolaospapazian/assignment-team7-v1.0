package com.team7.project.service;


import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;

import java.time.LocalDate;
import java.util.List;


/**
 * RepairService
 * Declare tha Methods to use Service With Repairs
 */
public interface RepairService {

    //TODO: only Dao Calls
    RepairDtoUpdate getRepairForUpdate(Long id);
    List<RepairDto> getRepairsList();
    RepairDto getRepairs(Long id);
    void setNewRepair(RepairDtoCreate repairDtoCreate);
    List<RepairDto> findRepairsToShowToday();
    void updateRepair(RepairDtoUpdate repairDtoUpdate);
    void deleteRepair(Long id);
    List<RepairDto> findRepairsUpcoming ();
    List<RepairDto> findRepairsToShowUntilToday();
    List<RepairDto> findRepairsToShowSpecificDay(LocalDate specificDay);
    List<RepairDto> findRepairsByVehiclePlate (String plate);
    List<RepairDto> findRepairsByAfmAndDate (String afm, LocalDate date );
}
