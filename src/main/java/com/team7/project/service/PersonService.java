package com.team7.project.service;

import com.team7.project.models.dtoPerson.*;
import com.team7.project.models.person.Person;

import java.util.List;

/**
 * PersonService
 * Declare tha Methods to use Service With Persons
 */
public interface PersonService {

    //TODO: only Dao Calls
    List<PersonDto> getAllPersons();
    PersonDto getPersonById(Long id);
    PersonDtoByEmailOrAFM getDetailsFromPerson(Long afm, String email);
    void create(PersonDtoCreate personDtoCreate);
    void update(PersonDtoUpdate personDtoUpdate,Long id);
    void delete(Long id);
    List<PersonDtoAllAfm>  getAllAfm();
    Person getPersonAllByAfm(Long afm);
    Person getPersonAllByEmail(String email);
    /*PersonDtoByEmailOrAFM getPersonByIdForUpdate(PersonDtoUpdate personDtoUpdate);*/


}
