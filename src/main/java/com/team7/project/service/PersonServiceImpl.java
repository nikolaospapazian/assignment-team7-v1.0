package com.team7.project.service;

import com.team7.project.dao.DaoPersonCreateUpdate;
import com.team7.project.dao.DaoPersonGet;
import com.team7.project.exception.PersonNotFoundException;
import com.team7.project.models.dtoPerson.*;
import com.team7.project.models.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



/**
 * PersonServiceImpl implements the PersonService
 * used to get or set properties from ServiceRepositoryPersonsTransactionFunction
 * and with using the dtoPerson.${}.Class sent or get to Repository Persons
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private DaoPersonCreateUpdate personDAO;
    @Autowired
    private DaoPersonGet daoPersonGet;

    @Override
    public List<PersonDto> getAllPersons() {
        return daoPersonGet.getAllPersons();
    }

    @Override
    public List<PersonDtoAllAfm> getAllAfm() {
        return daoPersonGet.getAllAfm();
    }

    @Override
    public PersonDto getPersonById(Long id) {
        PersonDto ret = daoPersonGet.getPersonByIdDAO(id);
        if(ret!=null){
            return ret;
        }else {
            throw new PersonNotFoundException("Repair with Id: "+id+" not exist");
        }

    }

    /**
     * getDetailsFromPerson
     * call DAO To Get Value From Repository And Get PersonDtoByEmailOrAFM
     * @param afm ,email of person to want to find
     * @return PersonDtoByEmailOrAFM Primary keys of tha person
     */
    @Override
    public PersonDtoByEmailOrAFM getDetailsFromPerson(Long afm, String email) {
        if (afm != null  ){
                return daoPersonGet.getPersonByAfm(afm);

        } else if (email != null ){
               return daoPersonGet.findPersonByPersonEmail(email);
        }
        else {
            throw new PersonNotFoundException("Repair with afm: "+afm+" and Email:"+email+" not exist");
        }
    }

    //TODO: delete this
    public PersonDtoByEmailOrAFM getDetailsFromPersonByAFM(Long afm){
        if(afm!=null ) {
            return daoPersonGet.getPersonByAfm(afm);
        }else {
            throw new PersonNotFoundException("Repair with afm: "+afm+" not exist");
        }
    }
    //TODO: delete this
    public PersonDtoByEmailOrAFM getDetailsFromPersonByEmail(String email){
        if(email!=null) {
            return daoPersonGet.findPersonByPersonEmail(email);
        }else {
            throw new PersonNotFoundException("Repair with email: "+email+" not exist");
        }
    }



    @Override
    public void create(PersonDtoCreate personDtoCreate) {
        if(!personDAO.isExistCreate(personDtoCreate)){
            personDAO.create(personDtoCreate);
        }else {
            throw new PersonNotFoundException("Person with Afm: "+personDtoCreate.getPersonAFM()+" exist ");
        }
    }

    @Override
    public void update(PersonDtoUpdate personDtoUpdate, Long id) {
            personDtoUpdate.setId(id);
            personDAO.updateDao(personDtoUpdate);

    }

    @Override
    public void delete(Long id) {
        personDAO.deleteDao(id);
    }


    public Person getPersonAllByAfm(Long afm){
        Person person=personDAO.FindPersonByAfmAll(afm);
        if(person!=null){
            return person;
        }else {
            throw new PersonNotFoundException("Repair with afm: "+afm+" not exist");
        }

    }

    public Person getPersonAllByEmail(String email){
        Person person=personDAO.FindPersonByEmailAll(email);
        if(person!=null) {
            return person;
        }else {
            throw new PersonNotFoundException("Repair with Email: "+email+" not exist");
        }
    }
}