package com.team7.project.security;

import com.team7.project.models.dtoPerson.PersonDtoLogIn;
import com.team7.project.models.person.EnumPersonType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collections;


@Service
public class AuthenticationService {

     public PersonDtoLogIn login(String email, String password){
         return new PersonDtoLogIn(email, password, Collections.singletonList(new SimpleGrantedAuthority(EnumPersonType.USER.toString())));
     }


    public void logout(String email, String password){


    };


}
