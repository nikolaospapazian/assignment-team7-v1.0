package com.team7.project.security;

import com.team7.project.dao.DaoPersonGet;
import com.team7.project.dao.SetPersonDtos;
import com.team7.project.models.dtoPerson.PersonDtoLogIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class AccountLoginAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    DaoPersonGet daoPersonGet;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        PersonDtoLogIn loginResponse = authenticateUser(authentication);
        return new UsernamePasswordAuthenticationToken(loginResponse.getEmail(), loginResponse.getPassword(), loginResponse.getPersonType());
    }



    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private PersonDtoLogIn authenticateUser(Authentication authentication) {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        var person = daoPersonGet.getUserForLogIn(username, password);

        if(person==null){
            throw new BadCredentialsException("User or password incorrect");
        }else {
            SetPersonDtos setPersonDtos = new SetPersonDtos();
            return setPersonDtos.setPersonForLogIn(person);
        }

    }
}
