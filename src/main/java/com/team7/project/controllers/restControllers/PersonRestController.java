package com.team7.project.controllers.restControllers;

import com.team7.project.forms.SearchForm;
import com.team7.project.models.dtoPerson.PersonDto;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.dtoPerson.PersonDtoUpdate;
import com.team7.project.service.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonRestController {

    @Autowired
    private PersonServiceImpl personService;

    @GetMapping(path="/api/Person")
    public List<PersonDto> getPersons(){
        return personService.getAllPersons();
    }

//    @GetMapping(path="/admin/owners/api/search")
//    public PersonDtoByEmailOrAFM personSearch(@RequestParam (name="afm" , required = false, defaultValue = "") long afm,
//                                              @RequestParam (name="email" , required = false, defaultValue = "") String email){
//        return personService.getDetailsFromPerson(afm,email);
//    }


//    @GetMapping(path="/api/admin/owners/search")
//    public PersonDtoByEmailOrAFM personSearch( @RequestParam (name="afm" , required = false, defaultValue = "null") String afm,
//                                              @RequestParam (name="email" , required = false, defaultValue = "null") String email
//                                              ,@ModelAttribute SearchForm searchForm) {
//
//        if (afm.isEmpty() | afm.equals("null")) {
//            return personService.getDetailsFromPerson(null, email);
//        } else return personService.getDetailsFromPerson(Long.parseLong(afm), null);
//    }
    @GetMapping(path = "/api/admin/owners/search")
    public ResponseEntity<PersonDtoByEmailOrAFM> personSearch(@RequestParam(name = "afm", required = false, defaultValue = "") String afm,
                                                              @RequestParam(name = "email", required = false, defaultValue = "") String email
            , @ModelAttribute SearchForm searchForm) {

        try {
            if (afm.isEmpty() | afm.equals("null")) {
                return ResponseEntity.ok(personService.getDetailsFromPerson(null, email));
            } else return ResponseEntity.ok(personService.getDetailsFromPerson(Long.parseLong(afm), null));
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }







    @GetMapping(path="/admin/owners/api/search/afm")
    public PersonDtoByEmailOrAFM getPersonByAFM(@RequestParam (name="afm" , required = false, defaultValue = "") long afm){
        return personService.getDetailsFromPerson(afm,null);
    }

    @GetMapping(path="/admin/owners/api/search/email")
    public PersonDtoByEmailOrAFM getPersonByEmail(@RequestParam (name="email" , required = false, defaultValue = "")  String email){
        return personService.getDetailsFromPerson(null,email);
    }

    @GetMapping(path="/api/Person/afm/{afm}")
    public PersonDtoByEmailOrAFM getPersonByAFM(@PathVariable Long afm){
        return personService.getDetailsFromPerson(afm,null);
    }

//    @GetMapping(path="/api/Person/email/{email}")
//    public PersonDtoByEmailOrAFM getPersonByEmail(@PathVariable String email){
//        return personService.getDetailsFromPerson(null,email);
//    }
    //TODO:delete this
    @GetMapping(path="/api/Person/afm2/{afm}")
    public PersonDtoByEmailOrAFM getPersonByAFM2(@PathVariable Long afm){
        return personService.getDetailsFromPersonByAFM(afm);
    }

//    @GetMapping(path="/api/Person/email/{email}")
//    public PersonDtoByEmailOrAFM getPersonByEmail(@PathVariable String email){
//        return personService.getDetailsFromPerson(null,email);
//    }

    //TODO: delete this
    @GetMapping(path="/api/Person/email2/{email}")
    public PersonDtoByEmailOrAFM getPersonByEmail2(@PathVariable String email){
        return personService.getDetailsFromPersonByEmail(email);
    }

    @PostMapping(path="/api/Person/update/id/{id}")
    public int UpdateCustomPerson(@PathVariable Long id ,@RequestBody PersonDtoUpdate personDtoUpdate){
        if(id!=null & personDtoUpdate!=null) {
            personService.update(personDtoUpdate, id);
            return 200;
        }else {
            return 300;
        }
    }

    @PostMapping(path="/api/Person/create")
    public int AddCustomPerson(@RequestBody PersonDtoCreate personDtoCreate){
        if(personDtoCreate!=null) {
            personService.create(personDtoCreate);
            return 200;
        }else {
            return 300;
        }
    }

    @GetMapping("/api/Person/delete/{id}")
    public int DeleteCustomPerson(@PathVariable Long id){
        if(id!=null){
            personService.delete(id);
            return 200;
        }else{
            return 300;
        }

    }

    @GetMapping("/api/Person/search/{id}")
    public PersonDto SearchCustomPerson(@PathVariable Long id){
        return personService.getPersonById(id);
    }



}
