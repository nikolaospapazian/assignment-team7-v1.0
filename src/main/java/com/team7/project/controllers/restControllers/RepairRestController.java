package com.team7.project.controllers.restControllers;

import com.team7.project.exception.RepairNotfoundException;
import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.service.RepairServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Nikolaos Papazian
 * RepairRestController is e Rest Controller
 * Used to Return and Get Json Objects for Repairs
 * Called By PATH/api/...
 * And working Parallel with the rest Application
 */
@RestController
public class RepairRestController {

    @Autowired
    private RepairServiceImpl repairService;

    @GetMapping(path="/api/repair/all" )
    public List<RepairDto> repairList() {
        return repairService.getRepairsList();
    }

    @GetMapping(path="/api/repair/{id}")
    public RepairDto repairsListById(@PathVariable Long id){
        return repairService.getRepairs(id);
    }


    @GetMapping(path="/admin/repairs/api/search/afm")
    public List<RepairDto> repairsSearchListByAfm(@RequestParam (name="afm" , required = false, defaultValue = "") long afm){
        return repairService.findRepairByAfm(afm);
    }



    @GetMapping(path="/api/repair/today")
    public List<RepairDto> repairsListForToday(){
        return repairService.findRepairsToShowToday();
    }

    @GetMapping(path="/api/repair/UntilToday")
    public List<RepairDto> repairsListUntilToday(){
        return repairService.findRepairsToShowUntilToday();
    }

    @GetMapping(path="/api/repair/AfterToday")
    public List<RepairDto> repairsListAfterToday(){
        return repairService.findRepairsUpcoming();
    }

    @GetMapping(path="/api/repair/first")
    public List<RepairDto> repairsListFirst(){
        return repairService.getFirstTenRepairs();
    }

    @PostMapping(path="/api/insert/Repair")
    public int repairsInsertRepair(@RequestBody RepairDtoCreate repairDtoCreate){
        if(repairDtoCreate!= null) {
            repairService.setNewRepair(repairDtoCreate);
            return 200;
        }else{
            return 300;
        }
    }

    @PostMapping(path="/api/Update/Repair")
    public int repairsUpdateRepair(@RequestBody RepairDtoUpdate repairDtoUpdate){
        if(repairDtoUpdate!= null) {
            repairService.updateRepair(repairDtoUpdate);
            return 200;
        }else{
            return 300;
        }
    }



    @GetMapping(path="/api/admin/repairs/search")
    public ResponseEntity<List<RepairDto>> repairsListByAfm(@RequestParam (name="afm" , required = false, defaultValue = "null") String afm,
                                                      @RequestParam (name="dateFrom" , required = false, defaultValue = "") String date,
                                                      @RequestParam (name="SearchPlate" , required = false, defaultValue = "") String searchPlate) {
        List<RepairDto> list=repairService.searchRepairsByAfmAndByDateAndByPlate(afm, searchPlate, date);
        try {
            if(list.isEmpty()){
                throw new Exception("Bad request");
            }
                return ResponseEntity.ok(list);

        }catch (Exception ex){
            return ResponseEntity.notFound().build();
        }
    }


//        if (!afm.equals("null")) {
//            return repairService.findRepairByAfm(Long.parseLong(afm));
//        } else if (!searchPlate.isEmpty()){
//            return repairService.findRepairsByVehiclePlate(searchPlate);
//        }else if(!date.isEmpty()){return repairService.findRepairsToShowSpecificDay(LocalDate.parse(date));}
//        else throw new RepairNotfoundException("Bad Request");




    @GetMapping(path="/api/repair/VehiclePlate/{vehiclePlate}")
    public List<RepairDto> findRepairByVehiclePlate(@PathVariable String vehiclePlate){
        return repairService.findRepairsByVehiclePlate(vehiclePlate);
    }


    @GetMapping(path="/api/repair/specificDay/{day}")
    public List<RepairDto> repairsListForToday(@PathVariable String day){
        return repairService.findRepairsToShowSpecificDay(LocalDate.parse(day));
    }

    @GetMapping(path="/api/repair/owner")
    public List<RepairDto> repairsListForOwner(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return repairService.getRepairsByEmail(authentication.getName());
    }


}
