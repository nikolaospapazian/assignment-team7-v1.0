package com.team7.project.controllers.viewControllers;

import com.team7.project.exception.PersonNotFoundException;
import com.team7.project.forms.RepairRegisterForm;
import com.team7.project.mappers.RegisterFormToRepairMapper;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.service.RepairServiceImpl;
import com.team7.project.validators.RepairRegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.team7.project.utils.GlobalAttributes.ERROR_MESSAGE;
@Controller
public class RepairRegisterController {



    private static final String REGISTER_FORM = "repairRegisterForm";

    @Autowired
    private RepairServiceImpl repairServiceImpl;

    @Autowired
    private RepairRegisterValidator repairRegisterValidator;

    @Autowired
    private RegisterFormToRepairMapper mapper;

    @InitBinder(REGISTER_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(repairRegisterValidator);
    }


    @GetMapping(value = "/admin/repairs/register")
    public String register(Model model) {
        model.addAttribute(REGISTER_FORM,
                new RepairRegisterForm());
        return "RepairRegister";
    }


    @PostMapping(value = "/admin/repairs/register")
    public String register(Model model,
                           @Valid @ModelAttribute(REGISTER_FORM)
                                   RepairRegisterForm repairRegisterForm,
                           BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            //have some error handling here, perhaps add extra error messages to the model
            model.addAttribute(ERROR_MESSAGE, "an error occurred");
            return "RepairRegister";
        }
        try{
        RepairDtoCreate repairDtoCreate = mapper.repairDtoCreate(repairRegisterForm);
        repairServiceImpl.setNewRepair(repairDtoCreate);
        return "redirect:/admin/home";
        }catch (Exception ex) {
        model.addAttribute(REGISTER_FORM, repairRegisterForm);
        model.addAttribute("errorMessage", "Please enter a valid Afm.");
        return "RepairRegister";
    }
    }

    @ExceptionHandler({PersonNotFoundException.class})
    public String handleError(HttpServletRequest request, PersonNotFoundException e, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute(e);
        return "redirect:/admin/errorPage";
    }
}
