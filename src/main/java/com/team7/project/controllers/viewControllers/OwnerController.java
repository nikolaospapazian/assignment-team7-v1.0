package com.team7.project.controllers.viewControllers;


import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.service.RepairServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class OwnerController {

    @Autowired
    private RepairServiceImpl repairService;

    @GetMapping("/owner/Home")
    public String index(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        List<RepairDto> repairs = repairService.getRepairsByEmail(authentication.getName());

        model.addAttribute("repairs",repairs);

        return "OwnerHome";
    }
}
