package com.team7.project.controllers.viewControllers;


import com.team7.project.forms.RepairRegisterForm;
import com.team7.project.mappers.RepairToFormMapper;
import com.team7.project.mappers.UpdateFormtoRepairMapper;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.service.RepairServiceImpl;
import com.team7.project.validators.RepairRegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.team7.project.utils.GlobalAttributes.ERROR_MESSAGE;

@Controller
public class RepairUpdateController {


    private static final String REGISTER_FORM = "repairRegisterForm";
    @Autowired
    private RepairServiceImpl repairService;
    @Autowired
    private RepairRegisterValidator repairRegisterValidator;

    @Autowired
    private RepairToFormMapper mapperToForm;

    @Autowired
    private UpdateFormtoRepairMapper mapper;



    private static final String REPAIR_ATTR = "repair";

    @InitBinder(REGISTER_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(repairRegisterValidator);
    }



    @GetMapping("/admin/repair/edit/{id}")
    public String FindRepairForUpdate(@PathVariable Long id, Model model) {
        if (!model.asMap().containsKey(REGISTER_FORM)) {
            RepairDtoUpdate repair = repairService.getRepairForUpdate(id);
            repair.setsAfm(Long.toString(repair.getAfm()));
            RepairRegisterForm repairRegisterForm = mapperToForm.repairRegisterForm(repair);
            model.addAttribute(REGISTER_FORM, repairRegisterForm);
    }
        return "RepairEdit";
    }

    @PostMapping( "/admin/repair/edit/{id}")
    public String PostRepairUpdate(@PathVariable Long id, Model model,
                             @Valid @ModelAttribute(REGISTER_FORM)
                                     RepairRegisterForm repairRegisterForm,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
//            RepairDtoUpdate repair = repairService.getRepairForUpdate(id);
            model.addAttribute(ERROR_MESSAGE, "an error occurred");
//            model.addAttribute(REPAIR_ATTR,repair);
            return "RepairEdit";
        }
        try{
        RepairDtoUpdate repairDtoUpdate= mapper.repairDtoUpdate(repairRegisterForm);
        repairDtoUpdate.setId(id);
        repairService.updateRepair(repairDtoUpdate);
        return "redirect:/admin/repairs";
        }catch (Exception ex){
            model.addAttribute(REGISTER_FORM, repairRegisterForm);
            model.addAttribute("error111", "Couldn't update registration. Afm is not valid.");
            return "RepairEdit";
        }
    }
}
