package com.team7.project.controllers.viewControllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogOutController {

    @GetMapping("/logOut")
    public String logOutView(){
        return "";
    }

}
