package com.team7.project.controllers.viewControllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class NotFoundController implements ErrorController {

    @RequestMapping("/error")
    public String handleError() {
            return "redirect:/owner/Home";
    }

    @Override
    public String getErrorPath() {

        return "/error";

    }

}




