package com.team7.project.controllers.viewControllers;

import com.team7.project.service.PersonServiceImpl;
import com.team7.project.service.RepairServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DeleteController {

    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private RepairServiceImpl repairService;

    @GetMapping("/admin/owners/delete/{id}")
    public String DeleteCustomPerson(@PathVariable Long id){
        personService.delete(id);
        return "redirect:/admin/owners";
    }



    @GetMapping("/admin/repairs/delete/{id}")
    public String  DeleteCustomRepair(@PathVariable Long id){
        repairService.deleteRepair(id);
        return "redirect:/admin/home";
    }

}