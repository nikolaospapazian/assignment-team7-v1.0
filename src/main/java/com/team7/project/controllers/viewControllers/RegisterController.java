package com.team7.project.controllers.viewControllers;

import com.team7.project.exception.PersonNotFoundException;
import com.team7.project.forms.UserRegisterForm;
import com.team7.project.mappers.RegisterFormToUserMapper;
import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.person.Person;
import com.team7.project.service.PersonServiceImpl;
import com.team7.project.validators.RegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.team7.project.utils.GlobalAttributes.ERROR_MESSAGE;

@Controller
public class RegisterController {

   private static final String REGISTER_FORM = "userRegisterForm";

   @Autowired
   private PersonServiceImpl personServiceImpl;

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private RegisterFormToUserMapper mapper;

    @InitBinder(REGISTER_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(registerValidator);
    }


    @GetMapping("/admin/owners/register")
    public String register(Model model) {
        model.addAttribute(REGISTER_FORM,
                new UserRegisterForm());
        return "OwnerRegister";
    }


    @PostMapping(value = "/admin/owners/register")
    public String register(Model model,
                           @Valid @ModelAttribute(REGISTER_FORM)
                                   UserRegisterForm userRegisterForm,
                           BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            //have some error handling here, perhaps add extra error messages to the model
            model.addAttribute(ERROR_MESSAGE, "an error occurred");
                        return "OwnerRegister";
        }
        try{
        PersonDtoCreate personDtoCreate = mapper.personDtoCreate(userRegisterForm);
        personServiceImpl.create(personDtoCreate);
        return "redirect:/admin/owners";
        }catch (Exception ex) {
         model.addAttribute(REGISTER_FORM, userRegisterForm);
         model.addAttribute("errorMessage", "Registration could not be complete. Afm, Email and Licence Plate cannot be duplicate.");
         return "OwnerRegister";
}
    }

}
