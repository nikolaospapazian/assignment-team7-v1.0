package com.team7.project.controllers.viewControllers;


import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.service.RepairServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AdminController {

    private static final String REPAIRS_ATTR = "repairs";

    @Autowired
    private RepairServiceImpl repairService;

    @GetMapping("/admin/home")
    public String todayRepairs(Model model) {
         List<RepairDto> repairs = repairService.findRepairsToShowToday();

         model.addAttribute(REPAIRS_ATTR,repairs);
        return "AdminHome";
    }


    @GetMapping("/admin/owners")
    public String owners() {
        return "AdminOwners";
    }

    @GetMapping("/admin/repairs")
    public String repairs() {
        return "AdminRepairs";
    }


}

