package com.team7.project.controllers.viewControllers;

import com.team7.project.forms.UserRegisterForm;
import com.team7.project.mappers.PersonToFormMapper;
import com.team7.project.mappers.UpdateFormtoUserMapper;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.dtoPerson.PersonDtoUpdate;
import com.team7.project.service.PersonServiceImpl;
import com.team7.project.service.RepairServiceImpl;
import com.team7.project.validators.RegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.team7.project.utils.GlobalAttributes.ERROR_MESSAGE;

@Controller
public class UpdateController {

    private static final String REGISTER_FORM = "userRegisterForm";

    @Autowired
    private PersonServiceImpl personServiceImpl;

    @Autowired
    private RepairServiceImpl repairService;
    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private PersonToFormMapper mapper;

    @Autowired
    private UpdateFormtoUserMapper mapper1;

    @InitBinder(REGISTER_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(registerValidator);
    }

    private static final String PERSON_ATTR = "person";


    @GetMapping("/admin/owners/edit/{afm}")
    public String FindPersonForUpdate(@PathVariable Long afm, Model model) {

        if (!model.asMap().containsKey(REGISTER_FORM)) {
            PersonDtoByEmailOrAFM person = personServiceImpl.getDetailsFromPerson(afm, null);
            person.setAfm(Long.toString(person.getPersonAfm()));
            UserRegisterForm userRegisterForm = mapper.userRegisterForm(person);
            model.addAttribute(REGISTER_FORM, userRegisterForm);
        }
        return "OwnerEdit";
    }


    @PostMapping("/admin/owners/edit/{id}")
    public String PostUpdate(@PathVariable Long id, Model model,
                             @Valid @ModelAttribute(REGISTER_FORM)
                                     UserRegisterForm userRegisterForm,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
//            PersonDto person = personServiceImpl.getPersonById(id);
//            PersonDtoByEmailOrAFM person = personServiceImpl.(id);
//            model.addAttribute(PERSON_ATTR,person);
            //have some error handling here, perhaps add extra error messages to the model
            model.addAttribute(ERROR_MESSAGE, "an error occurred");
//            model.addAttribute(PERSON_ATTR, person);
            return "OwnerEdit";
        }
        try {
            PersonDtoUpdate personDtoUpdate = mapper1.personDtoUpdate(userRegisterForm);
            personServiceImpl.update(personDtoUpdate, id);
            return "redirect:/admin/owners";

        } catch (Exception ex) {
            model.addAttribute(REGISTER_FORM, userRegisterForm);
            model.addAttribute("error111", "Couldn't update registration. Afm, Email and Licence Plate cannot be duplicate.");
            return "OwnerEdit";
        }

    }

//    @ExceptionHandler({PersonNotFoundException.class})
//    public String handleError(HttpServletRequest request,
//                              RedirectAttributes redirectAttrs,
//                              RuntimeException e, Model model) {
//        redirectAttrs.addFlashAttribute(ERROR_MESSAGE, "Couldn't fetch data");
//        return "OwnerEdit";
//    }

}
