package com.team7.project.repository;

import com.team7.project.models.person.Person;
import com.team7.project.models.repair.Repair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * RepairRepository
 * @author Nikolaos Papazian
 * This Repository extends JpaRepository
 * And controlle tha Repair DB table
 */
@Repository
public interface RepairRepository extends JpaRepository<Repair, Long> {


    /**
     * findAllByRepairId
     * @return List OF All Repair Tasks
     */
    List<Repair> findAll();

    /**
     * findAllByRepairId
     * @param  id from Specific Task
     * @return this function return RepairTask By Specific ID
     */
    Repair findByRepairId(Long id);

    /**
     * findByPersonIs
     * @param person and
     * @return all repair for that person
     */
    List<Repair> findByPersonIs(Person person);


    /**
     * findAllRepairsByDayToBefore
     * @param date today
     * @return Repair List of all repair is not finis
     */
    List<Repair> findAllRepairsByDayToAfter(LocalDate date);

    /**
     * findTop10ByDayToAfter
     * @param date1 date2 today
     * @return Repair List Of first 10 Repairs order by day to is after today
     */
    List<Repair> findTop10ByDayToAfterAndAndDayFromBefore(LocalDate date1,LocalDate date2);
    /**
     * findRepairsByDayStartAfter
     * @param date1 , date2 today
     * @return Repair List with repairs to mast start before today and ends after that
     */
    List<Repair> findAllRepairsByDayStartBeforeAndDayToAfter(LocalDate date1, LocalDate date2);


    /**
     * findAllRepairsByDayStartAfter
     * @param date today
     * @return Repair List with Upcoming repairs
     */
    List<Repair> findAllRepairsByDayStartAfter(LocalDate date);

    /**findRepairsByPersonPersonIDContaining
     *
     * @param PersonID get the person ID and return tre Repair List
     * @return Repair List of specific person And All Of Repairs of his Car;
     */
    List<Repair> findRepairsByPersonPersonIDContaining(Long PersonID);


    List<Repair> findRepairsByPersonPersonAFMIsAndDayToIsAfterAndDayFromIsBefore (Long afm, LocalDate date1, LocalDate date2);

    List<Repair> findRepairsByPersonPersonVehiclePlateIsAndDayToIsAfterAndDayFromIsBefore (String plate, LocalDate date1,LocalDate date2);

    List<Repair> findAllByPersonPersonEmailIs(String email);





    //TODO: set method to add Repairs in to repository

}
