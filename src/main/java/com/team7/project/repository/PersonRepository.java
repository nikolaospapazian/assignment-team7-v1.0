package com.team7.project.repository;

import com.team7.project.models.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PersonRepository
 * @author Nikolaos Papazian
 * This Repository extends JpaRepository
 * And controlle tha Person DB table
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    /**
     * findByAdminID
     * @param id get Specific Person id
     * @return specific Person By id
     */
    Person findByPersonAFM(Long id);

    /**
     * findAll
     * @return List with all Persons
     */
    List<Person> findAll();

    Person findPersonByPersonAFM(Long afm);
    Person findPersonByPersonEmailEquals(String email);
    Person findPersonByPersonVehiclePlate(String Plate);
    Person findPersonByPersonID(Long id);

    Person findPersonByPersonEmailIsAndAndPersonPasswordIs(String email,String password);

}
