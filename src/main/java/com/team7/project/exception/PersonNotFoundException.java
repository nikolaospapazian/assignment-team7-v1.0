package com.team7.project.exception;

public class PersonNotFoundException extends RuntimeException {

    public PersonNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
