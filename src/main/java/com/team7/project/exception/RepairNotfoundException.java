package com.team7.project.exception;

public class RepairNotfoundException extends RuntimeException {
    public RepairNotfoundException(String errorMessage) {
        super(errorMessage);
    }
}
