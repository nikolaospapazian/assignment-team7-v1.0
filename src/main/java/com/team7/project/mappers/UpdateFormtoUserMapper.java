package com.team7.project.mappers;

import com.team7.project.forms.UserRegisterForm;
import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.dtoPerson.PersonDtoUpdate;
import com.team7.project.models.person.EnumPersonType;
import org.springframework.stereotype.Component;

@Component
public class UpdateFormtoUserMapper {



    public PersonDtoUpdate personDtoUpdate(UserRegisterForm userRegisterForm) {

        PersonDtoUpdate personDtoUpdate = new PersonDtoUpdate();
        personDtoUpdate.setPersonFName(userRegisterForm.getFirstname());
        personDtoUpdate.setPersonLName(userRegisterForm.getLastname());
        personDtoUpdate.setPersonAddress(userRegisterForm.getAddress());
        personDtoUpdate.setPassword(userRegisterForm.getPassword());
        personDtoUpdate.setPersonEmail(userRegisterForm.getEmail());
        personDtoUpdate.setPersonVehicleBrand(userRegisterForm.getVehicleBrand());
        personDtoUpdate.setPersonVehiclePlate(userRegisterForm.getLicencePlate());
        personDtoUpdate.setPersonType((userRegisterForm.getUserType()));
        personDtoUpdate.setPersonAFM(Long.parseLong(userRegisterForm.getAfm()));
        return personDtoUpdate;
    }
}

