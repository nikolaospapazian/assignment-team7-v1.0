package com.team7.project.mappers;

import com.team7.project.forms.RepairRegisterForm;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.models.repair.EnumFixType;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class RegisterFormToRepairMapper {



    public RepairDtoCreate repairDtoCreate(RepairRegisterForm repairRegisterForm) {

        RepairDtoCreate repairDtoCreate = new RepairDtoCreate();

        repairDtoCreate.setFix_type(repairRegisterForm.getFix_type());
        repairDtoCreate.setPrice(repairRegisterForm.getCost());
        repairDtoCreate.setDescription(repairRegisterForm.getDescription());
        repairDtoCreate.setDayFrom(LocalDate.parse(repairRegisterForm.getdateFrom()));
        repairDtoCreate.setAfm(Long.parseLong(repairRegisterForm.getAfm()));


        return repairDtoCreate;
    }
}
