package com.team7.project.mappers;

import com.team7.project.forms.UserRegisterForm;
import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.person.EnumPersonType;
import org.hibernate.usertype.UserType;
import org.springframework.stereotype.Component;

@Component
public class RegisterFormToUserMapper {



    public PersonDtoCreate personDtoCreate(UserRegisterForm userRegisterForm) {

        PersonDtoCreate personDtoCreate = new PersonDtoCreate();

        personDtoCreate.setPersonFName(userRegisterForm.getFirstname());
        personDtoCreate.setPersonLName(userRegisterForm.getLastname());
        personDtoCreate.setPersonAddress(userRegisterForm.getAddress());
        personDtoCreate.setPassword(userRegisterForm.getPassword());
        personDtoCreate.setPersonEmail(userRegisterForm.getEmail());
        personDtoCreate.setPersonVehicleBrand(userRegisterForm.getVehicleBrand());
        personDtoCreate.setPersonVehiclePlate(userRegisterForm.getLicencePlate());
        personDtoCreate.setPersonType((userRegisterForm.getUserType()));
        personDtoCreate.setPersonAFM(Long.parseLong(userRegisterForm.getAfm()));

        return personDtoCreate;
    }
}
