package com.team7.project.mappers;

import com.team7.project.forms.UserRegisterForm;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.person.EnumPersonType;
import org.springframework.stereotype.Component;

@Component
public class PersonToFormMapper {


    public UserRegisterForm userRegisterForm(PersonDtoByEmailOrAFM person) {

        UserRegisterForm userRegisterForm = new UserRegisterForm();

        userRegisterForm.setFirstname(person.getPersonFirstName());
        userRegisterForm.setLastname(person.getPersonLastName());
        userRegisterForm.setAddress(person.getPersonAddress());
        userRegisterForm.setEmail(person.getPersonEmail());
        userRegisterForm.setVehicleBrand(person.getVehicleBrand());
        userRegisterForm.setLicencePlate(person.getVehiclePlate());
        userRegisterForm.setAfm(person.getAfm());
        userRegisterForm.setId(person.getPersonId());
        userRegisterForm.setUserType(person.getPersonType());
        userRegisterForm.setPassword(person.getPassword());

        return userRegisterForm;

    }

}
