package com.team7.project.mappers;

import com.team7.project.forms.RepairRegisterForm;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.models.repair.EnumRepairStage;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class RepairToFormMapper {

    public RepairRegisterForm repairRegisterForm(RepairDtoUpdate repair) {

        RepairRegisterForm repairRegisterForm = new RepairRegisterForm();
        repairRegisterForm.setAfm(repair.getsAfm());
        repairRegisterForm.setDateFrom(repair.getDayFrom().toString());
        repairRegisterForm.setDateStart(repair.getDayStart().toString());
        repairRegisterForm.setDateTo(repair.getDayTo().toString());
        repairRegisterForm.setDescription(repair.getDescription());
        repairRegisterForm.setSprice(Double.toString(repair.getPrice()));
        repairRegisterForm.setId(repair.getId());
        repairRegisterForm.setStage((repair.getStage()));
        repairRegisterForm.setFix_type(repair.getFix_type());

        return repairRegisterForm;
    }
}