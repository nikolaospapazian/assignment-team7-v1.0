package com.team7.project.mappers;


import com.team7.project.forms.RepairRegisterForm;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.EnumRepairStage;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class UpdateFormtoRepairMapper {

    public RepairDtoUpdate repairDtoUpdate(RepairRegisterForm repairRegisterForm) {

        RepairDtoUpdate repairDtoUpdate = new RepairDtoUpdate();

        repairDtoUpdate.setFix_type((repairRegisterForm.getFix_type()));
        repairDtoUpdate.setPrice(Double.parseDouble(repairRegisterForm.getSprice().replace(",",".")));
        repairDtoUpdate.setStage(repairRegisterForm.getStage());
        repairDtoUpdate.setDayFrom(LocalDate.parse(repairRegisterForm.getdateFrom()));
        repairDtoUpdate.setDayStart(LocalDate.parse(repairRegisterForm.getDateStart()));
        repairDtoUpdate.setDayTo(LocalDate.parse(repairRegisterForm.getDateTo()));
        repairDtoUpdate.setDescription(repairRegisterForm.getDescription());
        repairDtoUpdate.setAfm(Long.valueOf(repairRegisterForm.getAfm()));
        /*repairDtoUpdate.setAfm(Long.valueOf(repairRegisterForm.getAfm()));
        repairDtoUpdate.setEmail(repairRegisterForm.);*/

        return repairDtoUpdate;
    }
}
