package com.team7.project.models.dtoPerson;

import com.team7.project.models.person.EnumPersonType;

public class PersonDtoUpdate {

    private Long id;
    private Long personAFM;
    private String personFName;
    private String personLName;
    private String personAddress;
    private String personEmail;
    private String personVehicleBrand;
    private String personVehiclePlate;
    private EnumPersonType personType;
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonAFM() {
        return personAFM;
    }

    public void setPersonAFM(Long personAFM) {
        this.personAFM = personAFM;
    }

    public String getPersonFName() {
        return personFName;
    }

    public void setPersonFName(String personFName) {
        this.personFName = personFName;
    }

    public String getPersonLName() {
        return personLName;
    }

    public void setPersonLName(String personLName) {
        this.personLName = personLName;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    public String getPersonVehicleBrand() {
        return personVehicleBrand;
    }

    public void setPersonVehicleBrand(String personVehicleBrand) {
        this.personVehicleBrand = personVehicleBrand;
    }

    public String getPersonVehiclePlate() {
        return personVehiclePlate;
    }

    public void setPersonVehiclePlate(String personVehiclePlate) {
        this.personVehiclePlate = personVehiclePlate;
    }

    public EnumPersonType getPersonType() {
        return personType;
    }

    public void setPersonType(EnumPersonType personType) {
        this.personType = personType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
