package com.team7.project.models.dtoPerson;

public class PersonDtoCheckCreate {
    private String email;
    private Long afm;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getAfm() {
        return afm;
    }

    public void setAfm(Long afm) {
        this.afm = afm;
    }
}
