package com.team7.project.models.dtoPerson;

import com.team7.project.models.person.EnumPersonType;

public class PersonDto {

    private Long personId;
    private Long personAfm;
    private String personEmail;
    private String personAddress;
    private String personFirstName;
    private String personLastName;
    private String vehicleBrand;
    private String vehiclePlate;
    private EnumPersonType personType;


    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }



    public EnumPersonType getPersonType() {
        return personType;
    }

    public void setPersonType(EnumPersonType personType) {
        this.personType = personType;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getPersonAfm() {
        return personAfm;
    }

    public void setPersonAfm(Long personAfm) {
        this.personAfm = personAfm;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }
}
