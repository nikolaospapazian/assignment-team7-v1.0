package com.team7.project.models.repair;



/**
* EnumRepairStage
* @author Nikolaos Papazian
* that enumerated calss conteins the Values For Repair State Type
*
* PAUSE or ON_ROAD or FINIS
*/
public enum EnumRepairStage {
    IN_QUEUE("IN_QUEUE"),
    IN_PROGRESS("IN_PROGRESS"),
    COMPLETED("COMPLETED");

    private String repairState;

    EnumRepairStage(String repairState) {
        this.repairState = repairState;
    }

    public String getRepairState() {
        return repairState;
    }
}
