package com.team7.project.models.repair;

public enum  EnumDayToFixVehicle {
    SMALLFIX(5),
    BIGFIX(10),
    START(1);

    private int fixDay;
    EnumDayToFixVehicle(int fixDay) {
        this.fixDay=fixDay;
    }

    public int getEnumDayToFixVehicle() {
        return fixDay;
    }
}
