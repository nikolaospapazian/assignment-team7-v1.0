package com.team7.project.models.repair;



/**
 * EnumFixType
 * @author Nikolaos Papazian
 * that enumerated calss conteins the Values For Repair Fix type
 *
 * SMALL or BIG
 */
public enum  EnumFixType {
    SMALL("SMALL"),
    BIG("BIG");

    private String fixType;
     EnumFixType(String fixType) {
        this.fixType=fixType;
    }

    public String getFixType() {
        return fixType;
    }
}
