package com.team7.project.models.repair;

import com.team7.project.models.person.Person;

import javax.persistence.*;
import java.time.LocalDate;


/**
 * Repair
 * @author Nikolaos Papazian
 * This class used by Spring Freamwork to
 * Create Database Table Repairs
 * and has all properties and geters-seters for that table
 */
@Entity
@Table(name="repair")
public class Repair {
    private static final int MAX_NAME_LENGTH  = 200;

    @Id
    @Column(name = "repair_id", nullable = false)
    @GeneratedValue(generator="increment",strategy = GenerationType.IDENTITY)
    private Long repairId;

    @Enumerated(EnumType.STRING)
    @Column(name = "repair_fix_type")
    private EnumFixType fix_type;

    @Enumerated(EnumType.STRING)
    @Column(name = "repair_stage")
    private EnumRepairStage stage;

    @Column(name = "repair_price")
    private double price;

    @Column(name="repair_from_day")
    private LocalDate dayFrom;

    @Column(name="repair_start_day")
    private LocalDate dayStart;

    @Column(name="repair_end_day")
    private LocalDate dayTo;

    @Column(name = "repair_description", length = MAX_NAME_LENGTH)
    private String description;

    @ManyToOne (optional = false /*,cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.REFRESH}*/)
    @JoinColumn(name = "person_id",referencedColumnName="person_id",nullable = false)
    private Person person;


    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    public EnumFixType getFix_type() {
        return fix_type;
    }

    public void setFix_type(EnumFixType fix_type) {
        this.fix_type = fix_type;
    }

    public EnumRepairStage getStage() {
        return stage;
    }

    public void setStage(EnumRepairStage stage) {
        this.stage = stage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getDayFrom() {
        return dayFrom;
    }

    public void setDayFrom(LocalDate dayFrom) {
        this.dayFrom = dayFrom;
    }

    public LocalDate getDayStart() {
        return dayStart;
    }

    public void setDayStart(LocalDate dayStart) {
        this.dayStart = dayStart;
    }

    public LocalDate getDayTo() {
        return dayTo;
    }

    public void setDayTo(LocalDate dayTo) {
        this.dayTo = dayTo;
    }
}
