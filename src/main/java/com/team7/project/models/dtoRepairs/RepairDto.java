package com.team7.project.models.dtoRepairs;


import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.EnumRepairStage;

import java.time.LocalDate;

/**
 * RepairDto
 * That class use to contains Repair properties
 * only For User
 * Transfer the properties From Repository To Service
 */
public class RepairDto {
    private Long id;
    private EnumFixType fix_type;
    private EnumRepairStage stage;
    private double price;
    private String description;
    private LocalDate dayStart;
    private String vehiclePlate;
    private Long personAfm;
    private String afm;
    private String firstName;
    private String lastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnumFixType getFix_type() {
        return fix_type;
    }

    public void setFix_type(EnumFixType fix_type) {
        this.fix_type = fix_type;
    }

    public EnumRepairStage getStage() {
        return stage;
    }

    public void setStage(EnumRepairStage stage) {
        this.stage = stage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDayStart() {
        return dayStart;
    }

    public void setDayStart(LocalDate dayStart) {
        this.dayStart = dayStart;
    }

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public Long getPersonAfm() {
        return personAfm;
    }

    public void setPersonAfm(Long personAfm) {
        this.personAfm = personAfm;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
