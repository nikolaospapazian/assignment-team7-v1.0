package com.team7.project.models.dtoRepairs;

import com.team7.project.models.repair.EnumFixType;

import java.time.LocalDate;

public class RepairDtoCreate {

    private EnumFixType fix_type;
    private String description;
    private Double price;
    private LocalDate dayFrom;
    private Long afm;
    private String email;


    public EnumFixType getFix_type() {
        return fix_type;
    }

    public void setFix_type(EnumFixType fix_type) {
        this.fix_type = fix_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getDayFrom() {
        return dayFrom;
    }

    public void setDayFrom(LocalDate dayFrom) {
        this.dayFrom = dayFrom;
    }

    public Long getAfm() {
        return afm;
    }

    public void setAfm(Long afm) {
        this.afm = afm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
