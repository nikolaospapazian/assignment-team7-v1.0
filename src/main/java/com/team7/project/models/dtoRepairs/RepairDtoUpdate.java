package com.team7.project.models.dtoRepairs;

import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.EnumRepairStage;

import java.time.LocalDate;

public class RepairDtoUpdate {
    private Long id;
    private EnumFixType fix_type;
    private EnumRepairStage stage;
    private double price;
    private LocalDate dayFrom;
    private LocalDate dayStart;
    private LocalDate dayTo;
    private String description;
    private Long afm;
    private String email;
    private String sAfm;
    private String sprice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnumFixType getFix_type() {
        return fix_type;
    }

    public void setFix_type(EnumFixType fix_type) {
        this.fix_type = fix_type;
    }

    public EnumRepairStage getStage() {
        return stage;
    }

    public void setStage(EnumRepairStage stage) {
        this.stage = stage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getDayFrom() {
        return dayFrom;
    }

    public void setDayFrom(LocalDate dayFrom) {
        this.dayFrom = dayFrom;
    }

    public LocalDate getDayStart() {
        return dayStart;
    }

    public void setDayStart(LocalDate dayStart) {
        this.dayStart = dayStart;
    }

    public LocalDate getDayTo() {
        return dayTo;
    }

    public void setDayTo(LocalDate dayTo) {
        this.dayTo = dayTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAfm() {
        return afm;
    }

    public void setAfm(Long afm) {
        this.afm = afm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getsAfm() {
        return sAfm;
    }

    public void setsAfm(String sAfm) {
        this.sAfm = sAfm;
    }

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }
}
