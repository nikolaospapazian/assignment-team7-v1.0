package com.team7.project.models.person;


/**
 * EnumPersonType
 * @author Nikolaos Papazian
 * that enumerated calss conteins the Values For Person Type
 *
 * ADMIN or USER
 */
public enum EnumPersonType {
    USER("User"),
    ADMIN("Admin");
    private String person ;

    EnumPersonType(String person) {
        this.person=person;
    }

}
