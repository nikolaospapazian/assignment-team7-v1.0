package com.team7.project.models.person;

import com.team7.project.models.repair.Repair;

import javax.persistence.*;
import java.util.List;


/**
 * Person Model
 * @author Nikolaos Papazian
 * This class used by Spring Freamwork to
 * Create Database Table Persons
 * and has all properties and geters-seters for that table
 */
@Entity
@Table(name="person")
public class Person {
    private static final int MAX_NAME_LENGTH  = 60;

    @Id
    @Column(name = "person_id", nullable = false)
    @GeneratedValue(generator="increment",strategy = GenerationType.IDENTITY)
    private Long personID;

    @Column(name = "person_afm", nullable = false , unique = true)
    private Long personAFM;

    @Column(name = "person_first_name", length = MAX_NAME_LENGTH)
    private String personFName;

    @Column(name = "person_last_name", length = MAX_NAME_LENGTH)
    private String personLName;

    @Column(name = "person_address",length = MAX_NAME_LENGTH)
    private String personAddress;

    @Column(name = "person_email",length = MAX_NAME_LENGTH, nullable = false , unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String personEmail;

    @Column(name = "person_pass",length = MAX_NAME_LENGTH)
    private String personPassword;

    @Column(name = "person_vehicle_brand",length = MAX_NAME_LENGTH)
    private String personVehicleBrand;

    @Column(name = "person_vehicle_plate",length = MAX_NAME_LENGTH , unique = true)
    private String personVehiclePlate;

    @Enumerated(EnumType.STRING)
    @Column(name = "personType", nullable = false)
    private EnumPersonType personType;
  /* @Column(name = "personType")
   private String personType;*/

    @OneToMany(cascade={CascadeType.REMOVE,CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval = true, mappedBy = "person", targetEntity =Repair.class)
    private List<Repair> repair;



    public Long getPersonID() {
        return personID;
    }

    public void setPersonID(Long personID) {
        this.personID = personID;
    }

    public Long getPersonAFM() {
        return personAFM;
    }

    public void setPersonAFM(Long personAFM) {
        this.personAFM = personAFM;
    }

    public String getPersonFName() {
        return personFName;
    }

    public void setPersonFName(String personFName) {
        this.personFName = personFName;
    }

    public String getPersonLName() {
        return personLName;
    }

    public void setPersonLName(String personLName) {
        this.personLName = personLName;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    public String getPersonPassword() {
        return personPassword;
    }

    public void setPersonPassword(String personPassword) {
        this.personPassword = personPassword;
    }

    public String getPersonVehicleBrand() {
        return personVehicleBrand;
    }

    public void setPersonVehicleBrand(String personVehicleBrand) {
        this.personVehicleBrand = personVehicleBrand;
    }

    public String getPersonVehiclePlate() {
        return personVehiclePlate;
    }

    public void setPersonVehiclePlate(String personVehiclePlate) {
        this.personVehiclePlate = personVehiclePlate;
    }

    public EnumPersonType getPersonType() {
        return personType;
    }

    public void setPersonType(EnumPersonType personType) {
        this.personType = personType;
    }

    public List<Repair> getRepair() {
        return repair;
    }

    public void setRepair(List<Repair> repair) {
        this.repair = repair;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + personID +
                ", AFM='" + personAFM + '\'' +
                ", Fname='" + personFName + '\'' +
                ", Lname=" + personLName +
                ", personType=" + personType +
                '}';
    }
}
