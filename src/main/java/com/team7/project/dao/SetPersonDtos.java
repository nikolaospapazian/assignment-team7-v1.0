package com.team7.project.dao;

import com.team7.project.models.dtoPerson.PersonDto;
import com.team7.project.models.dtoPerson.PersonDtoAllAfm;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.dtoPerson.PersonDtoLogIn;
import com.team7.project.models.person.Person;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class SetPersonDtos {

    public PersonDto setPersonDTO(Person person){
        PersonDto personDto = new PersonDto();

        personDto.setPersonId(person.getPersonID());
        personDto.setPersonAfm(person.getPersonAFM());
        personDto.setPersonEmail(person.getPersonEmail());
        personDto.setPersonFirstName(person.getPersonFName());
        personDto.setPersonLastName(person.getPersonLName());
        personDto.setPersonAddress(person.getPersonAddress());
        personDto.setPersonType(person.getPersonType());
        personDto.setVehicleBrand(person.getPersonVehicleBrand());
        personDto.setVehiclePlate(person.getPersonVehiclePlate());
        return personDto;
    }


    /**
     * setPersonDtoByEmailOrAfm
     * create PersonDtoByEmailOrAFM from Property Of Person
     * @param person From Repository
     * @return PersonDtoByEmailOrAFM the Details of Primary Key for That Person
     */
    public PersonDtoByEmailOrAFM setPersonDtoByEmailOrAfm(Person person){
        PersonDtoByEmailOrAFM personDtoByEmailOrAFM = new PersonDtoByEmailOrAFM();

        personDtoByEmailOrAFM.setPersonAfm(person.getPersonAFM());
        personDtoByEmailOrAFM.setPersonEmail(person.getPersonEmail());
        personDtoByEmailOrAFM.setPersonAddress(person.getPersonAddress());
        personDtoByEmailOrAFM.setPersonId(person.getPersonID());
        personDtoByEmailOrAFM.setPersonType(person.getPersonType());
        personDtoByEmailOrAFM.setVehicleBrand(person.getPersonVehicleBrand());
        personDtoByEmailOrAFM.setVehiclePlate(person.getPersonVehiclePlate());
        personDtoByEmailOrAFM.setPersonFirstName(person.getPersonFName());
        personDtoByEmailOrAFM.setPersonLastName(person.getPersonLName());
        personDtoByEmailOrAFM.setPassword(person.getPersonPassword());

        return personDtoByEmailOrAFM;
    }

    /**
     * setPersonForLogIn
     * @param person from Repository
     * @return PersonDtoLogIn with login Property
     */
    public PersonDtoLogIn setPersonForLogIn(Person person){
        PersonDtoLogIn personDtoLogIn =new PersonDtoLogIn(person.getPersonEmail(),person.getPersonPassword(),Collections.singletonList(new SimpleGrantedAuthority(person.getPersonType().toString())));
        return personDtoLogIn;
    }

    public PersonDtoAllAfm setPersonDtoAllAfm(Person person){
        PersonDtoAllAfm personDtoAllAfm = new PersonDtoAllAfm();
        personDtoAllAfm.setAfm(person.getPersonAFM());
        return personDtoAllAfm;
    }

    public List<PersonDtoAllAfm> setListOfDto(List<Person> personList){
        List<PersonDtoAllAfm> PersonDtoList = new ArrayList<>();
        if(personList!= null) {
            for (Person person : personList) {
                PersonDtoList.add(setPersonDtoAllAfm(person));
            }
        }
        return PersonDtoList;
    }
}
