package com.team7.project.dao;


import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoCreate;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.models.person.Person;
import com.team7.project.models.repair.EnumRepairStage;
import com.team7.project.models.repair.Repair;
import com.team7.project.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * ServiceRepositoryRepairsTransactionFunction
 * @author Nikolaos Papazian
 * This Class used to transfer data From and To RepositoryRepairs
 */
@Component
public class DaoRepairCreateUpdate {


    @Autowired
    private RepairRepository repairRepository;
    private SetRepairDtos setRepairDtos;



    //TODO: fix the first Ten
    /**
     * findFirstTen
     * @return RepairDto List
     */
    public List<RepairDto> findFirstTen(){
        setRepairDtos=new SetRepairDtos();
        List<Repair> TenRepair=repairRepository.findAll();
        List<RepairDto> repairDtoList=new ArrayList<>();
        if(TenRepair.size()>10){
            for(int i =1;i<=10;i++){
                repairDtoList.add(setRepairDtos.setRepairDTO(TenRepair.get(i)));
            }
        }

        return repairDtoList;
    }




    /**
     * setNewRepair
     * Set all Values To Insert New Repair In DataBase
     * @param repairDtoCreate Cum from Service and Contain the values From AdminPerson
     * @return to Repository a Fix Container with Values To insert In Database
     */
    public int setNewRepair(RepairDtoCreate repairDtoCreate, Person Person){
        setRepairDtos=new SetRepairDtos();
        Repair repair = new Repair();
        if(repairDtoCreate!=null || Person!=null) {
            repair.setFix_type(repairDtoCreate.getFix_type());
            repair.setStage(EnumRepairStage.IN_QUEUE);
            repair.setPrice(repairDtoCreate.getPrice());
            repair.setDescription(repairDtoCreate.getDescription());
            repair.setDayFrom(repairDtoCreate.getDayFrom()); // Admin set only the value of Current Day
            //the custom Function sets Day to Start one day after Vehicle cum the Repair Store
            repair.setDayStart(setRepairDtos.setDateTo(repairDtoCreate.getDayFrom(),null));
            // the custom function set the Day of expect to finis the Fixing from type of Fix
            repair.setDayTo(setRepairDtos.setDateTo(repairDtoCreate.getDayFrom(),repairDtoCreate.getFix_type()));
            repair.setPerson(Person);
        }
        try{
            Repair savedRepair = repairRepository.save(repair);
        }catch (Exception e){
            return 500;
        }
        return 200;
    }


    public void updateRepair(RepairDtoUpdate repairDtoUpdate,Person Person){
        if(repairDtoUpdate!=null ){
            if(repairRepository.getOne(repairDtoUpdate.getId())!=null) {

                Repair repair = repairRepository.findByRepairId(repairDtoUpdate.getId());
                repair.setFix_type(repairDtoUpdate.getFix_type());
                repair.setStage(repairDtoUpdate.getStage());
                repair.setPrice(repairDtoUpdate.getPrice());
                repair.setDescription(repairDtoUpdate.getDescription());
                repair.setDayFrom(repairDtoUpdate.getDayFrom());
                repair.setDayStart(repairDtoUpdate.getDayStart());
                repair.setDayTo(repairDtoUpdate.getDayTo());
                repair.setPerson(Person);

                repairRepository.save(repair);
            }
        }
    }

    public void deleteRepair(long id){
        repairRepository.delete(repairRepository.getOne(id));
    }


}
