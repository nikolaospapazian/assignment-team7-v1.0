package com.team7.project.dao;

import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.models.person.Person;
import com.team7.project.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;


@Component
public class DaoRepairGet {

    @Autowired
    private RepairRepository repairRepository;
    private SetRepairDtos setRepairDtos;



    /**
     * getRepairs
     * That function Called by Services and Return Data From Repository
     * @param id of Repair
     * @return RepairDto to give the values in service
     */
    public RepairDto getRepairs(Long id)
    {
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setRepairDTO(repairRepository.findByRepairId(id));
    }


    /**
     * getRepairsList
     * That function Called by Services and Return Data From Repository
     * @return RepairDto List to give the values in service
     */
    public List<RepairDto> getRepairsList(){
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findAll());
    }


    public List<RepairDto> getRepairsByEmail(String email){
        setRepairDtos=new SetRepairDtos();
       return setRepairDtos.setListOfDto(repairRepository.findAllByPersonPersonEmailIs(email));
    }

    /**
     * getRepairsByAfm
     * tha function search persons and after tha search repairs for that person
     * @param person Person Type
     * @return RepairDto List
     */
    public List<RepairDto> getRepairsByAfm(Person person){
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findByPersonIs(person));
    }

    /**
     * findRepairsAfterDay
     * @param date get today
     * @return RepairDto List with Repairs to mast end after today
     */
    public List<RepairDto> findRepairsBeforeDay(LocalDate date){
        setRepairDtos=new SetRepairDtos();
        /*return setRepairDtos.setListOfDto( repairRepository.findAllRepairsByDayToAfter(date));*/
        return setRepairDtos.setListOfDto( repairRepository.findTop10ByDayToAfterAndAndDayFromBefore(date,date));
    }

    /**
     * findRepairsAfterDay
     * @param date get today
     * @return RepairDto List with Repairs to mast start or continuous today
     */
    public List<RepairDto> findRepairsBeforeDayUntilToday(LocalDate date) {
        setRepairDtos = new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findAllRepairsByDayStartBeforeAndDayToAfter(date, date));
    }

    /**
     * findRepairsAfterDay
     * @param date get today
     * @return RepairDto List with Repairs to mast start from today and after
     */
    public List<RepairDto> findRepairsAfterDay(LocalDate date){
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findAllRepairsByDayStartAfter(date));
    }

    public RepairDtoUpdate getRepairForUpdate(Long id){
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setRepairDtoUpdate(repairRepository.findByRepairId(id));
    }


    public List<RepairDto> findRepairsByAfmAndDate (Long afm, LocalDate date){
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findRepairsByPersonPersonAFMIsAndDayToIsAfterAndDayFromIsBefore(afm,date,date));
    }

    public List<RepairDto> findRepairsByPlateAndDate (String plate, LocalDate date) {
        setRepairDtos=new SetRepairDtos();
        return setRepairDtos.setListOfDto(repairRepository.findRepairsByPersonPersonVehiclePlateIsAndDayToIsAfterAndDayFromIsBefore(plate,date,date));
    }
}
