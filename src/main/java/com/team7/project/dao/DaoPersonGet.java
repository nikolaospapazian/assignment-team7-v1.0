package com.team7.project.dao;

import com.team7.project.models.dtoPerson.PersonDto;
import com.team7.project.models.dtoPerson.PersonDtoAllAfm;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.person.Person;
import com.team7.project.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DaoPersonGet {


    @Autowired
    private PersonRepository personRepository;

    private SetPersonDtos setPersonDTOs;


    /**
     *
     * @return list with the person's information for services
     *
     */
    /*public List<PersonDto> getAllPersons(){
        setPersonDTOs =new SetPersonDtos();
        List<Person> persons= personRepository.findAll();
        List<PersonDto> personDtoList =new ArrayList<>();
        for(Person person:persons) {
            personDtoList.add(setPersonDTOs.setPersonDTO(person));
        }
        return personDtoList;
    }*/

    public List<PersonDto> getAllPersons(){

        setPersonDTOs =new SetPersonDtos();
        return personRepository
                .findAll()
                .stream()
                .map(Person-> setPersonDTOs.setPersonDTO(Person))
                .collect(Collectors.toList());
    }

    public PersonDto getPersonByIdDAO(Long id){
        setPersonDTOs= new SetPersonDtos();
       return setPersonDTOs.setPersonDTO(personRepository.findPersonByPersonID(id));
    }
    /**
     * getPersonByAfm
     * @param afm get the AFM Value from Person Service and Find Person
     * @return PersonDtoByEmailOrAFM person Details For Person with AFM==...
     */
    public PersonDtoByEmailOrAFM getPersonByAfm(Long afm){
        setPersonDTOs =new SetPersonDtos();
        return setPersonDTOs.setPersonDtoByEmailOrAfm(personRepository.findPersonByPersonAFM(afm));
    }

    /**
     * getPersonByAfm
     * @param email get the Email Value from Person Service and Find Person
     * @return PersonDtoByEmailOrAFM person Details For Person with Email==...
     */
    public PersonDtoByEmailOrAFM findPersonByPersonEmail (String email){
        setPersonDTOs =new SetPersonDtos();
        return setPersonDTOs.setPersonDtoByEmailOrAfm(personRepository.findPersonByPersonEmailEquals(email));
    }

    public List<PersonDtoAllAfm> getAllAfm(){
        PersonDtoAllAfm personDtoAllAfm =new PersonDtoAllAfm();
        return setPersonDTOs.setListOfDto(personRepository.findAll());
    }

    public Person getUserForLogIn(String email, String password){
        return personRepository.findPersonByPersonEmailIsAndAndPersonPasswordIs(email,password);
    }





}
