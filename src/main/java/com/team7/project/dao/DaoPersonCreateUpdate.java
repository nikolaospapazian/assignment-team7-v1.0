package com.team7.project.dao;


import com.team7.project.models.dtoPerson.PersonDtoCreate;
import com.team7.project.models.dtoPerson.PersonDtoUpdate;
import com.team7.project.models.person.Person;
import com.team7.project.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ServiceRepositoryPersonsTransactionFunction
 * @author Nikolaos Papazian
 * This Class used to transfer data From and To RepositoryPersosn
 */
@Component
public class DaoPersonCreateUpdate {



    @Autowired
    private PersonRepository personRepository;

    private SetPersonDtos setPersonDTOs;


/*
    public PersonDtoLogIn findPersonForLogIn(String email, String password) {
        setPersonDTOs =new SetPersonDtos();
        return setPersonDTOs.setPersonForLogIn(personRepository.findPersonByPersonEmailAndAndPersonPassword(email, password));
    }*/

    /**
     *
     * @param personDtoCreate
     * create a person for register
     */
    public void create (PersonDtoCreate personDtoCreate){

        Person person = new Person();

        person.setPersonPassword(personDtoCreate.getPassword());
        person.setPersonAFM(personDtoCreate.getPersonAFM());
        person.setPersonAddress(personDtoCreate.getPersonAddress());
        person.setPersonEmail(personDtoCreate.getPersonEmail());
        person.setPersonFName(personDtoCreate.getPersonFName());
        person.setPersonLName(personDtoCreate.getPersonLName());
        person.setPersonType(personDtoCreate.getPersonType());
        person.setPersonVehicleBrand(personDtoCreate.getPersonVehicleBrand());
        person.setPersonVehiclePlate(personDtoCreate.getPersonVehiclePlate());

        Person savedPerson = personRepository.save(person);
    }

    public boolean isExistCreate (PersonDtoCreate personDtoCreate){

        return isExist(personDtoCreate.getPersonAFM(),personDtoCreate.getPersonEmail(),personDtoCreate.getPersonVehiclePlate());
    }
    public boolean isExistUpdate (PersonDtoUpdate personDtoUpdate){

        return isExist(personDtoUpdate.getPersonAFM(),personDtoUpdate.getPersonEmail(),personDtoUpdate.getPersonVehiclePlate());
    }

    private boolean isExist (Long afm, String email , String plate ){
        if(personRepository.findPersonByPersonAFM(afm) != null ||
                personRepository.findPersonByPersonEmailEquals(email) !=null ||
                personRepository.findPersonByPersonVehiclePlate(plate)!=null)
        {
            return true;
        }
        return false;
    }




    /**
     * FindPersonByAfmAll
     * @param afm get afm and
     * @return User Object only for Create New Repair
     */
    public Person FindPersonByAfmAll(Long afm){
        return personRepository.findPersonByPersonAFM(afm);
    }
    /**
     * FindPersonByEmailAll
     * @param email get afm and
     * @return User Object only for Create New Repair
     */
    public Person FindPersonByEmailAll(String email){
        return personRepository.findPersonByPersonEmailEquals(email);
    }

    public void updateDao(PersonDtoUpdate personDtoUpdate){


        Person person = personRepository.findPersonByPersonID(personDtoUpdate.getId());

        person.setPersonID(personDtoUpdate.getId());
        person.setPersonAddress(personDtoUpdate.getPersonAddress());
        person.setPersonFName(personDtoUpdate.getPersonFName());
        person.setPersonPassword(personDtoUpdate.getPassword());
        person.setPersonAFM(personDtoUpdate.getPersonAFM());
        person.setPersonEmail(personDtoUpdate.getPersonEmail());
        person.setPersonLName(personDtoUpdate.getPersonLName());
        person.setPersonType(personDtoUpdate.getPersonType());
        person.setPersonVehicleBrand(personDtoUpdate.getPersonVehicleBrand());
        person.setPersonVehiclePlate(personDtoUpdate.getPersonVehiclePlate());

        personRepository.save(person);
    }

    public void deleteDao(Long id) {

        /*PersonDtoCreate personDtoCreate=new PersonDtoCreate();
        /*Person person = personRepository.findPersonByPersonID(id);*/
        personRepository.delete(personRepository.findPersonByPersonID(id));
    }

    public Person FindPersonByPlate(String plate){
        return personRepository.findPersonByPersonVehiclePlate(plate);
    }
}


