package com.team7.project.dao;

import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.dtoRepairs.RepairDtoUpdate;
import com.team7.project.models.repair.EnumDayToFixVehicle;
import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.Repair;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.team7.project.models.repair.EnumDayToFixVehicle.BIGFIX;
import static com.team7.project.models.repair.EnumDayToFixVehicle.SMALLFIX;
import static com.team7.project.models.repair.EnumFixType.BIG;
import static com.team7.project.models.repair.EnumFixType.SMALL;

public class SetRepairDtos {

    /**
     * setRepairDTO
     * Set get from Repository Data Type Repair
     * And return Data Type RepairDto
     * @param repair With All Values
     * @return RepairDto with specific Values for the View
     */
    protected RepairDto setRepairDTO(Repair repair){
        RepairDto repairDto = new RepairDto();
        repairDto.setId(repair.getRepairId());
        repairDto.setDayStart(repair.getDayStart());
        repairDto.setDescription(repair.getDescription());
        repairDto.setFix_type(repair.getFix_type());
        repairDto.setPrice(repair.getPrice());
        repairDto.setStage(repair.getStage());
        repairDto.setVehiclePlate(repair.getPerson().getPersonVehiclePlate());
        repairDto.setPersonAfm(repair.getPerson().getPersonAFM());
        repairDto.setFirstName(repair.getPerson().getPersonFName());
        repairDto.setLastName(repair.getPerson().getPersonLName());
        return repairDto;
    }

    /**
     * setDateTo
     * @param dt get date to Vehicle cum to Repair Store and Set day is Ready to Leave
     * @return 5 or 10 Days After The Custom Day To Repair the Vehicle
     * @return 1 Day For Starting to Fixing the Vehicle
     */
    protected LocalDate setDateTo(LocalDate dt, EnumFixType fixType){
        LocalDate outpout = dt;
        if(fixType==SMALL) {
            outpout=dt.plusDays(SMALLFIX.getEnumDayToFixVehicle());
        }else if(fixType==BIG) {
            outpout=dt.plusDays(BIGFIX.getEnumDayToFixVehicle());
        }else {
            outpout=dt.plusDays(EnumDayToFixVehicle.START.getEnumDayToFixVehicle());
        }
        return outpout;
    }


    protected List<RepairDto> setListOfDto(List<Repair> repairList){
        List<RepairDto> repairDtoList = new ArrayList<>();
        if(repairList!= null) {
            for (Repair repair : repairList) {
                repairDtoList.add(setRepairDTO(repair));
            }
        }
        return repairDtoList;
    }

    protected RepairDtoUpdate setRepairDtoUpdate(Repair repair){

        RepairDtoUpdate repairDtoUpdate = new RepairDtoUpdate();

        repairDtoUpdate.setId(repair.getRepairId());
        repairDtoUpdate.setDayStart(repair.getDayStart());
        repairDtoUpdate.setDescription(repair.getDescription());
        repairDtoUpdate.setFix_type(repair.getFix_type());
        repairDtoUpdate.setPrice(repair.getPrice());
        repairDtoUpdate.setStage(repair.getStage());
        repairDtoUpdate.setDayTo(repair.getDayTo());
        repairDtoUpdate.setDayFrom(repair.getDayFrom());
        repairDtoUpdate.setAfm(repair.getPerson().getPersonAFM());


        return repairDtoUpdate;
    }
}
