package com.team7.project.forms;

import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.EnumRepairStage;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RepairRegisterForm {

    private static final String AFM_PATTERN = "^[0-9]*$";
    private static final String DATE_PATTERN ="([0-9]{4})-{1}([0-9]{2})-{1}([0-9]{2})";
    private static final String NUMBER_PATTERN = "^[0-9]*$";
    private static final String DOUBLE_PATTERN = /*"^(\\d+([.,]d+)?)$"*/"[0-9]+([,.][0-9]{1,2})?";


    @Pattern(regexp = DATE_PATTERN, message = "Wrong date format")
    private String dateFrom;

    @Pattern(regexp = DATE_PATTERN, message = "Wrong date format")
    private String dateStart;

    @Pattern(regexp = DATE_PATTERN, message = "Wrong date format")
    private String dateTo;

    @Pattern(regexp = NUMBER_PATTERN, message="Afm should only be numbers.")
    @Size(min = 1, max=10, message = "Afm should be between 1 and 10 characters.")
    private String afm;

    @DecimalMin("0")
    private double cost;

    @Pattern(regexp = DOUBLE_PATTERN, message = "Wrong double format(give two or one decimal please)")
    private String sprice;

    private String description;
    private EnumFixType fix_type;
    private EnumRepairStage stage;
    private Long id;

    public String getdateFrom() {
        return dateFrom;
    }

    public void setdateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost){
        this.cost = cost;
    }

    public static String getAfmPattern() {
        return AFM_PATTERN;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public EnumFixType getFix_type() {
        return fix_type;
    }

    public void setFix_type(EnumFixType fix_type) {
        this.fix_type = fix_type;
    }

    public EnumRepairStage getStage() {
        return stage;
    }

    public void setStage(EnumRepairStage stage) {
        this.stage = stage;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getSprice() {
        return sprice;
    }

    public void setSprice(String sprice) {
        this.sprice = sprice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
