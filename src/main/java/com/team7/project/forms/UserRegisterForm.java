package com.team7.project.forms;

import com.team7.project.models.person.EnumPersonType;

import javax.validation.constraints.*;

public class UserRegisterForm {


    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]*$";

    private static final String NUMBER_PATTERN = "^[0-9]*$";

    private static final String PASSWORD_PATTERN = "^[a-zA-Z0-9@#$%^&]*$";
    private static final String MAIL_PATTERN = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$";
    private static final int PASSWORD_MINSIZE = 6;
    private static final String LICENCE_PLATE_PATTERN = "[A-Z]{3}[-]{1}[0-9]{4}";
    private static final String VEHICLE_BRAND_PATTERN = "^[a-zA-Z0-9 ]*$";
    // private static final String AFM_PATTERN = ("^\\d+$");
    private Long id;

    private String address;
    private EnumPersonType userType;

    @Pattern(regexp = USERNAME_PATTERN, message = "The firstname cannot be empty")
    private String firstname;

    @Pattern(regexp = USERNAME_PATTERN, message = "The lastname cannot be empty.")

    private String lastname;

    @Pattern(regexp = NUMBER_PATTERN, message="Afm should only be numbers")
    @Size(min = 1, max=10, message = "Afm should be between 1 and 10 digits.")
    private String afm;


    @Pattern(regexp = PASSWORD_PATTERN, message = "Invalid Password")
    @Size(min = PASSWORD_MINSIZE, message = "Password should be at least 6 characters.")
    private String password;

    @Pattern(regexp = MAIL_PATTERN, message = "Invalid mail format.")
    private String email;

    @Pattern(regexp = LICENCE_PLATE_PATTERN, message="Invalid Licence plate format.")
    private String licencePlate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Pattern(regexp = VEHICLE_BRAND_PATTERN, message = "Invalid Brand Name")
    private String vehicleBrand;

    public EnumPersonType getUserType() {
        return userType;
    }

    public void setUserType(EnumPersonType userType) {
        this.userType = userType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAfm() {
        return afm;
    }

    public void setAfm(String afm) {
        this.afm = afm;
    }

}
