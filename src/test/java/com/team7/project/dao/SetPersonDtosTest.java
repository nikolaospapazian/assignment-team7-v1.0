package com.team7.project.dao;

import com.team7.project.models.dtoPerson.PersonDto;
import com.team7.project.models.dtoPerson.PersonDtoByEmailOrAFM;
import com.team7.project.models.dtoPerson.PersonDtoLogIn;
import com.team7.project.models.person.EnumPersonType;
import com.team7.project.models.person.Person;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SetPersonDtosTest {

    @org.junit.jupiter.api.Test
    void setPersonDTO() {

        SetPersonDtos setPersonDtos=new SetPersonDtos();
        PersonDto personDtoTest=setPersonDtos.setPersonDTO(testPerson());

        assertTrue(personDtoTest.getPersonAfm()==(1234567L));
        assertTrue(personDtoTest.getPersonFirstName().equals("FirstName"));

        assertFalse(personDtoTest.getPersonAfm()==(0L));
        assertFalse(personDtoTest.getPersonLastName().equals("LastTruName"));


    }

    @org.junit.jupiter.api.Test
    void setPersonDtoByEmailOrAfm() {


        SetPersonDtos setPersonDtos=new SetPersonDtos();
        PersonDtoByEmailOrAFM personDtoByEmailOrAFM= setPersonDtos.setPersonDtoByEmailOrAfm(testPerson());

        assertTrue(personDtoByEmailOrAFM.getPersonAfm()==(1234567L));
        assertTrue(personDtoByEmailOrAFM.getPersonFirstName().equals("FirstName"));

        assertFalse(personDtoByEmailOrAFM.getPersonAfm()==(0L));
        assertFalse(personDtoByEmailOrAFM.getPersonFirstName().equals("LastName"));

    }

    @org.junit.jupiter.api.Test
    void setPersonForLogIn() {

        SetPersonDtos setPersonDtos=new SetPersonDtos();
        PersonDtoLogIn personDtoLogIn= setPersonDtos.setPersonForLogIn(testPerson());

        assertTrue(personDtoLogIn.getEmail().equals("Test@test.com"));
        assertTrue(personDtoLogIn.getPassword().equals("12345"));

        assertFalse(personDtoLogIn.getEmail().equals(""));
        assertFalse(personDtoLogIn.getPassword().equals("LastName"));

    }


    private Person testPerson(){
        Person personTest=new Person();
        personTest.setPersonPassword("12345");
        personTest.setPersonAFM(1234567L);
        personTest.setPersonAddress("L.Alexandras 33");
        personTest.setPersonEmail("Test@test.com");
        personTest.setPersonFName("FirstName");
        personTest.setPersonLName("LastName");
        personTest.setPersonType(EnumPersonType.ADMIN);
        personTest.setPersonVehicleBrand("Yamaha Mt-07");
        personTest.setPersonVehiclePlate("HBX-0551");
        return personTest;
    }

}