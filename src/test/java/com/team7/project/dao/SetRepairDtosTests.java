package com.team7.project.dao;

import com.team7.project.models.dtoRepairs.RepairDto;
import com.team7.project.models.person.EnumPersonType;
import com.team7.project.models.person.Person;
import com.team7.project.models.repair.EnumFixType;
import com.team7.project.models.repair.EnumRepairStage;
import com.team7.project.models.repair.Repair;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SetRepairDtosTests {

    @Test
    void setRepairDTO() {



        SetRepairDtos setRepairDtos=new SetRepairDtos();
        RepairDto repairDto= setRepairDtos.setRepairDTO(repairTest());
        assertTrue(repairDto.getPersonAfm()==(1234567L));
        assertTrue(repairDto.getPrice()==(109.90D));


        assertFalse(repairDto.getDayStart()==(LocalDate.parse("2017-12-12")));
        assertFalse(repairDto.getStage()==EnumRepairStage.COMPLETED);
    }

    @Test
    void setDateTo() {


        SetRepairDtos setRepairDtos=new SetRepairDtos();
        LocalDate dateToSet= setRepairDtos.setDateTo(LocalDate.parse("2017-12-12"), EnumFixType.SMALL);

        assertTrue(dateToSet.isEqual(LocalDate.parse("2017-12-17")));
        assertFalse(dateToSet.isEqual(LocalDate.parse("2017-12-12")));


        LocalDate dateToSet2= setRepairDtos.setDateTo(LocalDate.parse("2017-12-12"), EnumFixType.BIG);
        assertTrue(dateToSet2.isEqual(LocalDate.parse("2017-12-22")));
        assertFalse(dateToSet2.isEqual(LocalDate.parse("2017-12-12")));

        LocalDate dateToSet3= setRepairDtos.setDateTo(LocalDate.parse("2017-12-12"), null);
        assertTrue(dateToSet3.isEqual(LocalDate.parse("2017-12-13")));
        assertFalse(dateToSet3.isEqual(LocalDate.parse("2017-12-12")));
    }

    @Test
    void setListOfDto() {

        List<Repair> ListForTest=new ArrayList<>();
        ListForTest.add(repairTest());
        ListForTest.add(repairTest());
        ListForTest.add(repairTest());
        ListForTest.add(repairTest());
        ListForTest.add(repairTest());



        SetRepairDtos setRepairDtos=new SetRepairDtos();
        List<RepairDto> repairDtoList= setRepairDtos.setListOfDto(ListForTest);

        assertTrue(repairDtoList.get(0).getPersonAfm()==(1234567L));
        assertTrue(repairDtoList.get(4).getPrice()==(109.90D));

        assertFalse(repairDtoList.get(0).getDayStart()==(LocalDate.parse("2017-12-12")));
        assertFalse(repairDtoList.get(4).getStage()==EnumRepairStage.COMPLETED);


    }

    private Repair repairTest(){
        Repair repairTest = new Repair();
        repairTest.setFix_type(EnumFixType.SMALL);
        repairTest.setStage(EnumRepairStage.IN_QUEUE);
        repairTest.setPrice(109.90D);
        repairTest.setDescription("Description");
        repairTest.setDayFrom(LocalDate.parse("2018-12-12"));
        repairTest.setDayStart(LocalDate.parse("2018-12-13"));
        repairTest.setDayTo(LocalDate.parse("2018-12-30"));
        repairTest.setPerson(testPerson());

        return repairTest;
    }

    private Person testPerson(){
        Person personTest=new Person();
        personTest.setPersonPassword("12345");
        personTest.setPersonAFM(1234567L);
        personTest.setPersonAddress("L.Alexandras 33");
        personTest.setPersonEmail("Test@test.com");
        personTest.setPersonFName("FirstName");
        personTest.setPersonLName("LastName");
        personTest.setPersonType(EnumPersonType.ADMIN);
        personTest.setPersonVehicleBrand("Yamaha Mt-07");
        personTest.setPersonVehiclePlate("HBX-0551");
        return personTest;
    }

}